package airport;

import dto.*;

public class AirportData {

    private AirportData() {
    }

    public static final Space AIRSPACE = new Space(10000, 5000, 10000);
    public static final Space LANDING_CORRIDOR = new Space(10000, 2000, 10000);
    public static final Runway RUNWAY_01 = new Runway("01-L", 3000, 0, 45, new FlightPattern(new Coordinate(9200, 2000, 8500), new Coordinate(500, 1500, 8500), new Coordinate(500, 1068, 6000), new Coordinate(1000, 979, 6000), new Coordinate(2000, 890, 6000), new Coordinate(2500, 801, 6000), new Coordinate(3000, 712, 6000), new Coordinate(3500, 623, 6000), new Coordinate(4000, 534, 6000), new Coordinate(4500, 445, 6000), new Coordinate(5000, 356, 6000), new Coordinate(5500, 267, 6000), new Coordinate(6000, 178, 6000), new Coordinate(6500, 89, 6000), new Coordinate(7000, 0, 6000), new Coordinate(9500, 0, 6000)));
    public static final Runway RUNWAY_02 = new Runway("02-R", 3000, 0, 45, new FlightPattern(new Coordinate(9200, 2000, 8500), new Coordinate(500, 1500, 8500), new Coordinate(500, 1068, 4000), new Coordinate(1000, 979, 4000), new Coordinate(2000, 890, 4000), new Coordinate(2500, 801, 4000), new Coordinate(3000, 712, 4000), new Coordinate(3500, 623, 4000), new Coordinate(4000, 534, 4000), new Coordinate(4500, 445, 4000), new Coordinate(5000, 356, 4000), new Coordinate(5500, 267, 4000), new Coordinate(6000, 178, 4000), new Coordinate(6500, 89, 4000), new Coordinate(7000, 0, 4000), new Coordinate(9500, 0, 4000)));
    public static final InitialPattern INITIAL_PATTERN = new InitialPattern();
    public static final HoldingPattern HOLDING_PATTERN = new HoldingPattern();
    public static final double NEAR_DESTINATION_DISTANCE = 10;

    public static FlightPattern getLandingPattern(int runwayNumber) {
        return switch (runwayNumber) {
            case 1 -> RUNWAY_01.getLandingPattern();
            case 2 -> RUNWAY_02.getLandingPattern();
            default -> throw new IllegalArgumentException("Invalid runway number");
        };
    }
}