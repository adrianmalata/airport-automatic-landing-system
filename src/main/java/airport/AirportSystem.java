package airport;

import dto.AirplaneStatus;
import airport.utilities.InitialPatternXZCalculator;
import airport.utilities.MessageManager;
import airport.utilities.StatusAnalyzer;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Command;
import dto.Coordinate;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static utilities.PropertiesUtilities.NUMBER_OF_AIRPLANES;

public class AirportSystem {
    private static final Logger LOGGER = LogManager.getLogger(AirportSystem.class);
    public static final String REJECTION_MESSAGE = "Airport: Sorry %s we're full, you're not allowed to land here.\n";
    private static final int TRAFFIC_LEVELS = 10;
    private final StatusAnalyzer statusAnalyzer;
    @Getter
    private final List<AirplaneStatus> allTraffic;

    //Levels: 5000, 4700, 4400, 4100, 3800, 3500, 3200, 2900, 2600, 2300
    @Getter
    private final List<List<AirplaneStatus>> trafficAtLevels;
    @Getter
    private List<Coordinate> airplaneInitialPatternXZ;

    public AirportSystem() {
        statusAnalyzer = new StatusAnalyzer(this);
        allTraffic = new CopyOnWriteArrayList<>();
        trafficAtLevels = createNewTrafficAtLevels();
    }

    public String getGreetingOrRejection(AirplaneStatus airplaneStatus) {
        String response;
        boolean thereIsNoMoreSpace = allTraffic.size() >= NUMBER_OF_AIRPLANES;

        if (thereIsNoMoreSpace) {
            response = rejectNewAirplane(airplaneStatus.getName());
            LOGGER.info("Airplane {} rejected due to lack of space", airplaneStatus.getName());
        } else {
            response = greetNewAirplane(airplaneStatus);
            addAirplaneToTraffic(airplaneStatus);
            LOGGER.info("Airplane {} added to traffic", airplaneStatus.getName());
        }

        return response;
    }

    public Command prepareCommand(String status) {
        AirplaneStatus airplaneStatus = airplane.utilities.MessageManager.parseAirplaneStatus(status);
        return statusAnalyzer.analyzeAirplaneStatus(airplaneStatus);
    }

    public void addAirplaneToTraffic(AirplaneStatus airplaneStatus) {
        allTraffic.add(airplaneStatus);
    }

    public void updateAirplaneInTraffic(AirplaneStatus airplaneStatus) {
        for (AirplaneStatus airplane : allTraffic) {
            boolean foundByName = airplane.equals(airplaneStatus);

            if (foundByName) {
                int airplaneIndex = allTraffic.indexOf(airplane);
                allTraffic.set(airplaneIndex, airplaneStatus);
            }
        }
    }

    public void removeAirplaneFromTraffic(AirplaneStatus airplaneStatus) {
        allTraffic.remove(airplaneStatus);
        LOGGER.info("Airplane {} no longer in traffic", airplaneStatus.getName());
    }

    public void addAirplaneToTrafficAtHoldingPattern(int level, AirplaneStatus airplaneStatus) {
        if (trafficAtLevels.get(level).size() <= TRAFFIC_LEVELS) {
            trafficAtLevels.get(level).add(airplaneStatus);
        }
    }

    public void removeAirplaneFromTrafficAtHoldingPattern(AirplaneStatus airplaneStatus) {
        int level = getLayerLevel(airplaneStatus);
        trafficAtLevels.get(level).remove(airplaneStatus);
    }

    public int getLayerLevel(AirplaneStatus airplaneStatus) {
        List<AirplaneStatus> trafficInLayer = getTrafficInLayerWithAirplane(airplaneStatus);
        return trafficAtLevels.indexOf(trafficInLayer);
    }

    public List<AirplaneStatus> getTrafficInLayerWithAirplane(AirplaneStatus airplaneStatus) {
        List<AirplaneStatus> trafficInLayer;
        try {
             trafficInLayer = trafficAtLevels
                    .stream()
                    .filter(list -> list.contains(airplaneStatus))
                    .findFirst()
                    .orElseThrow();
        } catch (NoSuchElementException e) {
            trafficInLayer = trafficAtLevels.get(0);
        }

        return trafficInLayer;
    }

    public static double calculateDistanceBetweenCoordinates(Coordinate firstCoordinate, Coordinate secondCoordinate) {
        double powX = Math.pow(secondCoordinate.x() - firstCoordinate.x(), 2);
        double powY = Math.pow(secondCoordinate.y() - firstCoordinate.y(), 2);
        double powZ = Math.pow(secondCoordinate.z() - firstCoordinate.z(), 2);
        return Math.sqrt(powX + powY + powZ);
    }

    private String rejectNewAirplane(String airplaneName) {
        return String.format(REJECTION_MESSAGE, airplaneName);
    }

    private String greetNewAirplane(AirplaneStatus airplaneStatus) {
        Command initialCommand = statusAnalyzer.analyzeAirplaneStatus(airplaneStatus);
        airplaneInitialPatternXZ = InitialPatternXZCalculator.getInitialPatternXZ(airplaneStatus);
        return MessageManager.getCommandString(initialCommand);
    }

    private List<List<AirplaneStatus>> createNewTrafficAtLevels() {
        List<List<AirplaneStatus>> trafficAtAllLevels = new ArrayList<>();
        for (int i = 0; i < TRAFFIC_LEVELS; i++) {
            trafficAtAllLevels.add(new CopyOnWriteArrayList<>());
        }
        return trafficAtAllLevels;
    }
}