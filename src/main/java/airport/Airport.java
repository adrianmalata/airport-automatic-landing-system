package airport;

import static utilities.PropertiesUtilities.*;

public class Airport {
    private static final AirportServer AIRPORT_SERVER = new AirportServer();

    public static void main(String[] args) {
        AIRPORT_SERVER.runServer(MAIN_COMMUNICATION_PORT, SIMULATION_PORT);
        AIRPORT_SERVER.connectAirplanes();
        AIRPORT_SERVER.connectSimulation();
        AIRPORT_SERVER.handleAirplaneAndSimulationClientMessages();
        AIRPORT_SERVER.stopServer();
    }
}