package airport.utilities;

import dto.AirplaneStatus;
import airport.AirportSystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Command;
import dto.Coordinate;

import java.util.*;

import static airport.AirportData.*;
import static airport.AirportSystem.calculateDistanceBetweenCoordinates;

public class NextDestinationCalculator {
    private static final Logger LOGGER = LogManager.getLogger(NextDestinationCalculator.class);
    private final AirportSystem airportSystem;

    public NextDestinationCalculator(AirportSystem airportSystem) {
        this.airportSystem = airportSystem;
    }

    public Command getCommand(AirplaneStatus airplaneStatus) {
        boolean atDestination = airplaneStatus.isDestinationFlag();
        boolean collisionProcedureOn = airplaneStatus.isCollisionProcedureOn();

        if (atDestination) {
            return getNewDestination(airplaneStatus);
        } else if (collisionProcedureOn) {
            return proceedCollisionAvoidance(airplaneStatus);
        } else {
            return flyFurtherToDestination(airplaneStatus);
        }
    }

    private Command getNewDestination(AirplaneStatus airplaneStatus) {
        Coordinate nextDestination = getDestinationBeforeOrAfterFirstHoldingFix(airplaneStatus);
        double nextVelocity = getNextVelocity(airplaneStatus);
        return new Command(
                airplaneStatus.getName(),
                airplaneStatus.getCurrentPosition(),
                nextDestination,
                nextVelocity,
                airplaneStatus.isCollisionProcedureOn(),
                false);
    }

    private Command proceedCollisionAvoidance(AirplaneStatus airplaneStatus) {
        double nextVelocity = getNextVelocity(airplaneStatus);
        return new Command(
                airplaneStatus.getName(),
                airplaneStatus.getCurrentPosition(),
                airplaneStatus.getDestination(),
                nextVelocity,
                airplaneStatus.isCollisionProcedureOn(),
                false);
    }

    private Command flyFurtherToDestination(AirplaneStatus airplaneStatus) {
        double correctVelocity = correctVelocity(airplaneStatus);
        return new Command(
                airplaneStatus.getName(),
                airplaneStatus.getCurrentPosition(),
                airplaneStatus.getDestination(),
                correctVelocity,
                airplaneStatus.isCollisionProcedureOn(),
                false);
    }

    private double correctVelocity(AirplaneStatus airplaneStatus) {
        boolean airplaneInTrafficAtLevels = isAirplaneInTrafficAtLevels(airplaneStatus);

        if (airplaneInTrafficAtLevels) {
            return getNextVelocity(airplaneStatus);
        } else {
            return airplaneStatus.getVelocity();
        }
    }

    private boolean isAirplaneInTrafficAtLevels(AirplaneStatus airplaneStatus) {
        return airportSystem
                .getTrafficAtLevels()
                .stream()
                .flatMap(Collection::stream)
                .toList()
                .contains(airplaneStatus);
    }

    private Coordinate getDestinationBeforeOrAfterFirstHoldingFix(AirplaneStatus airplaneStatus) {
        try {
            return getDestinationAfterFirstHoldingFix(airplaneStatus);
        } catch (IllegalArgumentException airplaneNotInLayers) {
            return getDestinationBeforeFirstHoldingFix(airplaneStatus);
        }
    }

    private Coordinate getDestinationAfterFirstHoldingFix(AirplaneStatus airplaneStatus) {
        int indexOfLayerListWhereAirplaneIs = getTrafficLevelIndex(airplaneStatus);
        boolean thereIsAnotherLayer = indexOfLayerListWhereAirplaneIs + 1 < 10;
        List<Coordinate> currentHoldingPatternXZ = getHoldingPatternXZ(indexOfLayerListWhereAirplaneIs);
        boolean runwayOccupiedByAirplane = isRunwayOccupiedByAirplane(airplaneStatus, 1) || isRunwayOccupiedByAirplane(airplaneStatus, 2);

        if (thereIsAnotherLayer) {
            return getDestinationonHoldingPatternOnProperLayer(airplaneStatus, indexOfLayerListWhereAirplaneIs, currentHoldingPatternXZ);

        } else if (isLandingCorridorFullyOccupiedByOtherAirplanes(airplaneStatus) || (!airplaneIsAtHoldingFixCoordinate(airplaneStatus) && !runwayOccupiedByAirplane)) {
            double lastLayerHeight = HOLDING_PATTERN.getLayers().get(HOLDING_PATTERN.getLayers().size() - 1);
            return getDestinationOnHoldingPattern(airplaneStatus, currentHoldingPatternXZ, lastLayerHeight);

        } else {
            int runwayNumber;
            if (runwayOccupiedByAirplane) {
                runwayNumber = getRunwayNumber(airplaneStatus);
            } else {
                runwayNumber = RUNWAY_01.isOccupied() ? 2 : 1;
                occupyLandingCorridor(airplaneStatus, runwayNumber);
            }

            List<Coordinate> landingPattern = getLandingPattern(runwayNumber).getList();
            return getDestinationOnLandingPattern(airplaneStatus, landingPattern);
        }
    }

    private Coordinate getDestinationonHoldingPatternOnProperLayer(AirplaneStatus airplaneStatus, int indexOfLayerListWhereAirplaneIs, List<Coordinate> currentHoldingPatternXZ) {
        Coordinate destinationXZ = new Coordinate(airplaneStatus.getDestination().x(), Double.NaN, airplaneStatus.getDestination().z());
        boolean airplaneIsAtLastPointOnHoldingPattern = destinationXZ.equals(currentHoldingPatternXZ.get(3));
        List<Coordinate> nextHoldingPatternXZ = getHoldingPatternXZ(indexOfLayerListWhereAirplaneIs + 1);
        boolean nextLayerHasSlot = checkIfLayerHasSpot(indexOfLayerListWhereAirplaneIs + 1);

        if (nextLayerHasSlot && airplaneIsAtLastPointOnHoldingPattern) {
            moveAirplaneToNextLayer(indexOfLayerListWhereAirplaneIs, airplaneStatus);
            double nextHeight = HOLDING_PATTERN.getLayers().get(indexOfLayerListWhereAirplaneIs + 1);
            return getDestinationOnHoldingPattern(airplaneStatus, nextHoldingPatternXZ, nextHeight);
        } else {
            double height = HOLDING_PATTERN.getLayers().get(indexOfLayerListWhereAirplaneIs);
            return getDestinationOnHoldingPattern(airplaneStatus, currentHoldingPatternXZ, height);
        }
    }

    private int getTrafficLevelIndex(AirplaneStatus airplaneStatus) {
        for (int index = 0; index < airportSystem.getTrafficAtLevels().size(); index++) {
            List<AirplaneStatus> list = airportSystem.getTrafficAtLevels().get(index);
            if (list.contains(airplaneStatus)) {
                return index;
            }
        }
        throw new IllegalArgumentException();
    }

    private List<Coordinate> getHoldingPatternXZ(int indexOfLayerListWhereAirplaneIs) {
        boolean airplaneIsAbove4300 = indexOfLayerListWhereAirplaneIs <= 2;

        if (airplaneIsAbove4300) {
            return HOLDING_PATTERN.getAbove4300().getList();
        } else {
            return HOLDING_PATTERN.getBelow4300().getList();
        }
    }

    private boolean checkIfLayerHasSpot(int indexOfLayerList) {
        if (indexOfLayerList <= airportSystem.getTrafficAtLevels().size()) {
            long numberOfAirplanesInLayer = airportSystem.getTrafficAtLevels().get(indexOfLayerList).size();
            return numberOfAirplanesInLayer <= 10;
        }
        LOGGER.warn("Layer {} doesn't have more place", indexOfLayerList);
        return false;
    }

    private void moveAirplaneToNextLayer(int indexOfLayerListWhereAirplaneIs, AirplaneStatus airplaneStatus) {
        airportSystem.removeAirplaneFromTrafficAtHoldingPattern(airplaneStatus);
        airportSystem.addAirplaneToTrafficAtHoldingPattern(indexOfLayerListWhereAirplaneIs + 1, airplaneStatus);
    }

    private Coordinate getDestinationOnHoldingPattern(AirplaneStatus airplaneStatus, List<Coordinate> holdingPatternXZ, double height) {
        int index = getCoordinatePatternXZIndex(airplaneStatus.getDestination(), holdingPatternXZ);
        int nextHoldingPatternIndex = (index + 1) % holdingPatternXZ.size();
        return new Coordinate(holdingPatternXZ.get(nextHoldingPatternIndex).x(), height, holdingPatternXZ.get(nextHoldingPatternIndex).z());
    }

    private int getCoordinatePatternXZIndex(Coordinate coordinate, List<Coordinate> holdingPatternXZ) {
        Coordinate coordinateXZ = new Coordinate(coordinate.x(), Double.NaN, coordinate.z());

        for (int i = 0; i < holdingPatternXZ.size(); i++) {
            if (coordinateXZ.equals(holdingPatternXZ.get(i))) {
                return i;
            }
        }
        return -1;
    }

    private boolean isLandingCorridorFullyOccupiedByOtherAirplanes(AirplaneStatus airplaneStatus) {
        boolean runway01 = RUNWAY_01.isOccupied() && !RUNWAY_01.getOccupyingAirplane().equals(airplaneStatus);
        boolean runway02 = RUNWAY_02.isOccupied() && !RUNWAY_02.getOccupyingAirplane().equals(airplaneStatus);
        return runway01 && runway02;
    }

    private boolean airplaneIsAtHoldingFixCoordinate(AirplaneStatus airplaneStatus) {
        Coordinate holdingFixXZ = HOLDING_PATTERN.getHoldingFix();
        Coordinate holdingFix = new Coordinate(holdingFixXZ.x(), airplaneStatus.getCurrentPosition().y(), holdingFixXZ.z());
        double distance = calculateDistanceBetweenCoordinates(airplaneStatus.getCurrentPosition(), holdingFix);
        return distance < NEAR_DESTINATION_DISTANCE;
    }

    private boolean isRunwayOccupiedByAirplane(AirplaneStatus airplaneStatus, int runwayNumber) {
        if (runwayNumber == 1) {
            return RUNWAY_01.isOccupied() && RUNWAY_01.getOccupyingAirplane().equals(airplaneStatus);
        } else {
            return RUNWAY_02.isOccupied() && RUNWAY_02.getOccupyingAirplane().equals(airplaneStatus);
        }
    }

    private int getRunwayNumber(AirplaneStatus airplaneStatus) {
        return RUNWAY_01.getOccupyingAirplane().equals(airplaneStatus) ? 1 : 2;
    }

    private void occupyLandingCorridor(AirplaneStatus airplaneStatus, int runwayNumber) {
        if (runwayNumber == 1) {
            RUNWAY_01.setOccupied(true);
            RUNWAY_01.setOccupyingAirplane(airplaneStatus);
        } else if (runwayNumber == 2) {
            RUNWAY_02.setOccupied(true);
            RUNWAY_02.setOccupyingAirplane(airplaneStatus);
        }
    }

    private Coordinate getDestinationOnLandingPattern(AirplaneStatus airplaneStatus, List<Coordinate> landingPattern) {
        int index = getCurrentDestinationPatternXYZIndex(airplaneStatus.getDestination(), landingPattern);
        int nextLandingPatternIndex = (index + 1) % landingPattern.size();
        return landingPattern.get(nextLandingPatternIndex);
    }

    private int getCurrentDestinationPatternXYZIndex(Coordinate destination, List<Coordinate> pattern) {
        for (int i = 0; i < pattern.size(); i++) {
            if (destination.equals(pattern.get(i))) {
                return i;
            }
        }
        return -1;
    }

    private Coordinate getDestinationBeforeFirstHoldingFix(AirplaneStatus airplaneStatus) {
        int index = getCoordinatePatternXZIndex(airplaneStatus.getDestination(), airportSystem.getAirplaneInitialPatternXZ());

        if (index == airportSystem.getAirplaneInitialPatternXZ().size() - 1) {
            airportSystem.addAirplaneToTrafficAtHoldingPattern(0, airplaneStatus);
            return new Coordinate(HOLDING_PATTERN.getHoldingFix().x(), HOLDING_PATTERN.getLayers().get(0), HOLDING_PATTERN.getHoldingFix().z());
        }

        int nextDestinationBeforeHoldingFix = (index + 1) % airportSystem.getAirplaneInitialPatternXZ().size();
        double nextDestinationX = airportSystem.getAirplaneInitialPatternXZ().get(nextDestinationBeforeHoldingFix).x();
        double nextDestinationZ = airportSystem.getAirplaneInitialPatternXZ().get(nextDestinationBeforeHoldingFix).z();
        return new Coordinate(nextDestinationX, airplaneStatus.getCurrentPosition().y(), nextDestinationZ);
    }

    private double getNextVelocity(AirplaneStatus airplaneStatus) {
        double currentHeight = airplaneStatus.getCurrentPosition().y();
        if (currentHeight > 4300) {
            return HOLDING_PATTERN.getVelocityAbove4300();
        } else if (currentHeight <= 4300 && currentHeight > 1000) {
            return HOLDING_PATTERN.getVelocityBelow4300();
        } else if (currentHeight <= 1000 && currentHeight > 100) {
            return RUNWAY_01.getLandingApproachVelocity();
        } else {
            return RUNWAY_01.getLandingTouchdownVelocity();
        }
    }
}
