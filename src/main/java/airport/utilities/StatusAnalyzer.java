package airport.utilities;

import dto.AirplaneStatus;
import airport.AirportSystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Command;

import java.util.*;

import static airport.AirportData.RUNWAY_01;
import static airport.AirportData.RUNWAY_02;

public class StatusAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger(StatusAnalyzer.class);
    private final AirportSystem airportSystem;
    private final CollisionHandler collisionHandler;
    private final NextDestinationCalculator nextDestinationCalculator;


    public StatusAnalyzer(AirportSystem airportSystem) {
        this.airportSystem = airportSystem;
        collisionHandler = new CollisionHandler(airportSystem);
        nextDestinationCalculator = new NextDestinationCalculator(airportSystem);
    }

    public Command analyzeAirplaneStatus(AirplaneStatus airplaneStatus) {
        airportSystem.updateAirplaneInTraffic(airplaneStatus);
        List<AirplaneStatus> airplanesNearby = collisionHandler.scanForOtherAirplanesNearby(airplaneStatus);
        Map<AirplaneStatus, Boolean> possibleCollisions = collisionHandler.scanForCollisions(airplaneStatus, airplanesNearby);
        boolean canBeCollision = possibleCollisions.containsValue(true);
        boolean hasLanded = airplaneStatus.isLandedFlag();
        return getCommand(airplaneStatus, canBeCollision, possibleCollisions, hasLanded);
    }

    private Command getCommand(AirplaneStatus airplaneStatus, boolean canBeCollision, Map<AirplaneStatus, Boolean> possibleCollisions, boolean hasLanded) {
        Command command;
        if (canBeCollision) {
            boolean isCollision = collisionHandler.isCollision(airplaneStatus, possibleCollisions);

            if (isCollision) {
                LOGGER.info("Airplane {} crashed", airplaneStatus.getName());
                airportSystem.removeAirplaneFromTraffic(airplaneStatus);
                airportSystem.removeAirplaneFromTrafficAtHoldingPattern(airplaneStatus);
                command = collisionHandler.getCrashedCommand(airplaneStatus);

            } else {
                LOGGER.warn("Airplane {} can have collision", airplaneStatus.getName());
                command = collisionHandler.preventCollision(airplaneStatus);
            }
        } else if (hasLanded) {
            LOGGER.info("Airplane {} landed", airplaneStatus.getName());
            airportSystem.removeAirplaneFromTraffic(airplaneStatus);
            airportSystem.removeAirplaneFromTrafficAtHoldingPattern(airplaneStatus);
            setRunwayOccupiedFalse(airplaneStatus);
            command = getCommandWhileLanded(airplaneStatus);
        } else {
            command = nextDestinationCalculator.getCommand(airplaneStatus);
        }
        return command;
    }

    private Command getCommandWhileLanded(AirplaneStatus airplaneStatus) {
        return new Command(
                airplaneStatus.getName(),
                airplaneStatus.getCurrentPosition(),
                airplaneStatus.getDestination(),
                0,
                false,
                false);
    }

    private void setRunwayOccupiedFalse(AirplaneStatus airplaneStatus) {
        if (airplaneStatus.equals(RUNWAY_01.getOccupyingAirplane())) {
            RUNWAY_01.setOccupied(false);
        } else {
            RUNWAY_02.setOccupied(false);
        }
    }

}