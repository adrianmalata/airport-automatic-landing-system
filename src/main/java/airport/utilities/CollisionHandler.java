package airport.utilities;

import dto.AirplaneStatus;
import airplane.AirplaneSystem;
import airport.AirportSystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Command;
import dto.Coordinate;

import java.util.*;

import static airport.AirportData.NEAR_DESTINATION_DISTANCE;
import static airport.AirportSystem.calculateDistanceBetweenCoordinates;

public class CollisionHandler {
    private static final Logger LOGGER = LogManager.getLogger(CollisionHandler.class);
    private final AirportSystem airportSystem;

    public CollisionHandler(AirportSystem airportSystem) {
        this.airportSystem = airportSystem;
    }

    public List<AirplaneStatus> scanForOtherAirplanesNearby(AirplaneStatus airplane) {
        return airportSystem.getAllTraffic()
                .stream()
                .filter(airplaneFromTraffic -> !airplaneFromTraffic.equals(airplane))
                .filter(airplaneFromTraffic -> areAirplanesCloserThanOneSecond(airplane, airplaneFromTraffic))
                .toList();
    }

    public Map<AirplaneStatus, Boolean> scanForCollisions(AirplaneStatus airplaneStatus, List<AirplaneStatus> airplanesNearby) {
        Map<AirplaneStatus, Boolean> possibleCollisions = new HashMap<>();

        for (AirplaneStatus airplaneNearby : airplanesNearby) {
            calculatePossibleCollisions(possibleCollisions, airplaneStatus, airplaneNearby);
        }

        return possibleCollisions;
    }

    public Command preventCollision(AirplaneStatus airplaneStatus) {
        LOGGER.info("Airplane {} launched preventing collision procedure, velocity set to {}", airplaneStatus.getName(), airplaneStatus.getVelocity());

        return new Command(
                airplaneStatus.getName(),
                airplaneStatus.getCurrentPosition(),
                airplaneStatus.getDestination(),
                airplaneStatus.getVelocity() * 0.8,
                true,
                false);
    }

    public boolean isCollision(AirplaneStatus airplaneStatus, Map<AirplaneStatus, Boolean> possibleCollisions) {
        List<AirplaneStatus> possibleCollisionAirplanes = getPossibleCollisionAirplaneStatuses(possibleCollisions);
        List<AirplaneStatus> collisionAirplanes = getCollisionAirplanes(airplaneStatus, possibleCollisionAirplanes);
        return !collisionAirplanes.isEmpty();
    }

    public Command getCrashedCommand(AirplaneStatus airplaneStatus) {
        return new Command(
                airplaneStatus.getName(),
                airplaneStatus.getCurrentPosition(),
                airplaneStatus.getDestination(),
                0,
                false,
                true);
    }

    private void calculatePossibleCollisions(Map<AirplaneStatus, Boolean> possibleCollisions, AirplaneStatus airplaneStatus, AirplaneStatus airplaneNearby) {
        AirplaneSystem airplaneSystem = AirplaneSystem.getAirplaneSystemFromAirplaneStatus(airplaneStatus);
        List<Coordinate> oneSecondNextPositionsOfAirplane = airplaneSystem.getOneSecondNextPositions();
        Coordinate oneSecondNextPositionOfAirplaneNearby = airplaneNearby.getCurrentPosition();

        List<Double> futureDistancesBetweenAirplanes = calculateManyDistancesBetweenCoordinates(oneSecondNextPositionsOfAirplane, oneSecondNextPositionOfAirplaneNearby);
        double futureDistanceBetweenAirplanes = Collections.min(futureDistancesBetweenAirplanes);

        possibleCollisions.put(airplaneNearby, futureDistanceBetweenAirplanes <= 10);
    }

    private List<AirplaneStatus> getPossibleCollisionAirplaneStatuses(Map<AirplaneStatus, Boolean> possibleCollisions) {
        return possibleCollisions
                .entrySet()
                .stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .toList();
    }

    private List<AirplaneStatus> getCollisionAirplanes(AirplaneStatus airplaneStatus, List<AirplaneStatus> possibleCollisionAirplanes) {
        return possibleCollisionAirplanes.stream().filter(collisionAirplane -> {
            Coordinate airplaneCurrentPosition = airplaneStatus.getCurrentPosition();
            Coordinate collisionAirplaneCurrentPosition = collisionAirplane.getCurrentPosition();
            double distance = calculateDistanceBetweenCoordinates(airplaneCurrentPosition, collisionAirplaneCurrentPosition);
            return distance < NEAR_DESTINATION_DISTANCE;
        }).toList();
    }

    private boolean areAirplanesCloserThanOneSecond(AirplaneStatus airplaneStatus, AirplaneStatus airplaneFromTraffic) {
        double oneSecondDistance = getOneSecondDistance(airplaneStatus);
        return calculateDistanceBetweenCoordinates(
                airplaneStatus.getCurrentPosition(),
                airplaneFromTraffic.getCurrentPosition()) < oneSecondDistance;
    }

    private static double getOneSecondDistance(AirplaneStatus airplaneStatus) {
        return airplaneStatus.getVelocity() * 100;
    }

    private List<Double> calculateManyDistancesBetweenCoordinates(List<Coordinate> coordinatesList, Coordinate coordinate) {
        List<Double> distances = new ArrayList<>();

        for (Coordinate firstPosition : coordinatesList) {
            double distance = calculateDistanceBetweenCoordinates(firstPosition, coordinate);
            distances.add(distance);
        }

        return distances;
    }
}