package airport.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Command;

public class MessageManager {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule(new Jdk8Module());
    private static final Logger LOGGER = LogManager.getLogger(MessageManager.class);

    private MessageManager() {
    }

    public static String getCommandString(Command command) {
        ObjectNode preparedCommand = OBJECT_MAPPER.createObjectNode();

        preparedCommand.set("name", TextNode.valueOf(command.getName()));
        preparedCommand.set("destination", OBJECT_MAPPER.valueToTree(command.getDestination()));
        preparedCommand.set("velocity", DoubleNode.valueOf(command.getVelocity()));
        preparedCommand.set("collisionProcedureOn", OBJECT_MAPPER.valueToTree(command.isCollisionProcedureOn()));
        preparedCommand.set("crashed", BooleanNode.valueOf(command.isCrashed()));

        return preparedCommand.toString();
    }

    public static Command parseCommand(String command) {
        try {
            return OBJECT_MAPPER.readValue(command, Command.class);
        } catch (JsonProcessingException notCommand) {
            LOGGER.error("JsonProcessingException: Tried to parse command from JSON for command {} but this might not be a command", command);
            throw new IllegalStateException();
        }
    }
}