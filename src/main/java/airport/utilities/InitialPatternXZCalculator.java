package airport.utilities;

import dto.AirplaneStatus;
import dto.Coordinate;
import dto.FlightPattern;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static airport.AirportData.INITIAL_PATTERN;

public class InitialPatternXZCalculator {

    private InitialPatternXZCalculator() {
    }

    public static List<Coordinate> getInitialPatternXZ(AirplaneStatus airplaneStatus) {
        Coordinate destination = airplaneStatus.getDestination();
        double roundedHeight = airplaneStatus.getCurrentPosition().y();

        Map<FlightPattern, Coordinate> quadrantPoints = new LinkedHashMap<>();

        quadrantPoints.put(
                INITIAL_PATTERN.getTopLeft(),
                new Coordinate(
                        INITIAL_PATTERN.getTopLeft().getList().get(0).x(),
                        roundedHeight,
                        INITIAL_PATTERN.getTopLeft().getList().get(0).z()
                ));

        quadrantPoints.put(
                INITIAL_PATTERN.getTopRight(),
                new Coordinate(
                        INITIAL_PATTERN.getTopRight().getList().get(0).x(),
                        roundedHeight,
                        INITIAL_PATTERN.getTopRight().getList().get(0).z()
                ));

        quadrantPoints.put(
                INITIAL_PATTERN.getBottomLeft(),
                new Coordinate(
                        INITIAL_PATTERN.getBottomLeft().getList().get(0).x(),
                        roundedHeight,
                        INITIAL_PATTERN.getBottomLeft().getList().get(0).z()
                ));

        return quadrantPoints
                .keySet()
                .stream()
                .filter(pattern -> destination.equals(quadrantPoints.get(pattern)))
                .findFirst()
                .orElse(INITIAL_PATTERN.getBottomRight())
                .getList();
    }
}