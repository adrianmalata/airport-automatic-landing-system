package airport;

import database.DatabaseService;
import dto.AirplaneStatus;
import dto.Command;
import database.entities.AirplaneEntity;
import database.entities.CrashEntity;
import database.entities.LandingEntity;
import networking.AirplaneCommunication;
import networking.IOCommunication;
import networking.Server;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;

import static airport.AirportSystem.REJECTION_MESSAGE;
import static airport.utilities.MessageManager.getCommandString;
import static airplane.utilities.MessageManager.parseAirplaneStatus;
import static java.lang.Thread.sleep;
import static simulation.utilities.MessageManager.getPointPositionString;

public class AirportServer extends Server {
    private static final Logger LOGGER = LogManager.getLogger(AirportServer.class);
    private final AirportSystem airportSystem;
    private final DatabaseService databaseService = DatabaseService.getInstance();

    public AirportServer() {
        airportSystem = new AirportSystem();
    }

    public void connectSimulation() {
        simulationClientSocket = createClientSocket(simulationServerSocket);
        simulationCommunication = createCommunication(simulationClientSocket);
        LOGGER.info("Simulation communication working on local port {} (port {})", simulationClientSocket.getLocalPort(), simulationClientSocket.getPort());
    }

    public void introduceNewAirplane() {
        AirplaneCommunication airplaneCommunication = (AirplaneCommunication) getAirplaneCommunication();
        AirplaneStatus airplaneStatus = getAirplaneStatus(airplaneCommunication);

        boolean airplaneRejected = greetOrReject(airplaneCommunication, airplaneStatus);
        if (!airplaneRejected) {
            airplaneCommunication.setGreeted(true);
            updateSimulationForNewAirplane(airplaneStatus);
        }
    }

    public void handleAirplaneAndSimulationClientMessages() {
        int communicationsSize = 0;
        int updatedCommunicationsSize;

        introduceNewAirplane();

        while (areAirplanesToProcess()) {
            updatedCommunicationsSize = airplaneCommunications.size();
            delayProcessing();

            if (newAirplaneHasArrived(communicationsSize, updatedCommunicationsSize)) {
                introduceNewAirplane();
                communicationsSize = updatedCommunicationsSize;
                continue;
            }

            synchronizeWithAirplanes();
        }
    }

    private IOCommunication getAirplaneCommunication() {
        return getFromListWithRetry(airplaneCommunications, "airplaneCommunications", IOCommunication::isConnected);
    }

    private AirplaneStatus getAirplaneStatus(IOCommunication airplaneCommunication) {
        String airplaneInitialCall = readLineWithIOCommunication(airplaneCommunication);
        return parseAirplaneStatus(airplaneInitialCall);
    }

    private boolean greetOrReject(IOCommunication airplaneCommunication, AirplaneStatus airplaneStatus) {
        String response = airportSystem.getGreetingOrRejection(airplaneStatus);
        boolean airplaneRejected = response.contains(REJECTION_MESSAGE.replace("%s", airplaneStatus.getName()));
        sendMessageWithIOCommunication(airplaneCommunication, response);
        closeCommunicationIfAirplaneRejected(airplaneCommunication, airplaneRejected);
        return airplaneRejected;
    }

    private void closeCommunicationIfAirplaneRejected(IOCommunication airplaneCommunication, boolean airplaneRejected) {
        if (airplaneRejected) {
            airplaneCommunication.closeCommunication();
            airplaneCommunications.remove(airplaneCommunication);
        }
    }

    private void updateSimulationForNewAirplane(AirplaneStatus airplaneStatus) {
        if (airportSystem.getAllTraffic().contains(airplaneStatus)) {
            LOGGER.info("Traffic contains airplane {}", airplaneStatus.getName());
            updateSimulation(airplaneStatus);
        }
    }

    private void updateSimulation(AirplaneStatus airplaneStatus) {
        String messageToSimulation = getPointPositionString(airplaneStatus.getName(), airplaneStatus.getCurrentPosition(), airplaneStatus.isLandedFlag(), airplaneStatus.isCrashed());
        sendMessageWithIOCommunication(simulationCommunication, messageToSimulation);
    }

    private boolean areAirplanesToProcess() {
        return !airplaneCommunications.isEmpty() && !simulationCommunication.isClosed();
    }

    private void delayProcessing() {
        try {
            sleep(1);
        } catch (InterruptedException sleepInterrupted) {
            LOGGER.error("InterruptedException: Current thread {} interrupted", Thread.currentThread().getName());
            Thread.currentThread().interrupt();
        }
    }

    private static boolean newAirplaneHasArrived(int communicationsSize, int updatedCommunicationsSize) {
        return communicationsSize < updatedCommunicationsSize;
    }

    private void synchronizeWithAirplanes() {
        airplaneCommunications
                .stream()
                .filter(
                        airplaneCommunication -> airplaneCommunication instanceof AirplaneCommunication
                        && ((AirplaneCommunication) airplaneCommunication).isGreeted())
                .forEach(this::processAirplaneCommunication);
    }

    private void processAirplaneCommunication(IOCommunication airplaneCommunication) {
        String status = readLineWithIOCommunication(airplaneCommunication);

        if (status != null) {
            AirplaneStatus airplaneStatus = parseAirplaneStatus(status);
            Command command = airportSystem.prepareCommand(status);
            sendResponseToAirplane(airplaneCommunication, command);
            updateSimulation(airplaneStatus);
            disconnectAndAddRecordIfAirplaneMetCondition(airplaneCommunication, airplaneStatus, airplaneStatus.isCrashed(), () -> addCrashToDatabase(airplaneStatus));
            disconnectAndAddRecordIfAirplaneMetCondition(airplaneCommunication, airplaneStatus, airplaneStatus.isLandedFlag(), () -> addLandingToDatabase(airplaneStatus));
        }
    }

    private void sendResponseToAirplane(IOCommunication airplaneCommunication, Command command) {
        String responseToAirplane = getCommandString(command);
        sendMessageWithIOCommunication(airplaneCommunication, responseToAirplane);
    }

    private void disconnectAndAddRecordIfAirplaneMetCondition(IOCommunication airplaneCommunication, AirplaneStatus airplaneStatus, boolean condition, Runnable addToDatabaseAction) {
        if (condition) {
            disconnectIfAirplaneMetCondition(airplaneCommunication, airplaneStatus);
            addToDatabaseAction.run();
        }
    }

    private void disconnectIfAirplaneMetCondition(IOCommunication airplaneCommunication, AirplaneStatus airplaneStatus) {
        Command shutDownCommand = getShutDownCommand(airplaneStatus);
        String shutDownToAirplane = getCommandString(shutDownCommand);
        sendMessageWithIOCommunication(airplaneCommunication, shutDownToAirplane);
        airplaneCommunications.remove(airplaneCommunication);
        airplaneCommunication.closeCommunication();
    }

    private Command getShutDownCommand(AirplaneStatus airplaneStatus) {
        return new Command(
                airplaneStatus.getName(),
                airplaneStatus.getCurrentPosition(),
                airplaneStatus.getDestination(),
                -1,
                true,
                false);
    }

    private void addLandingToDatabase(AirplaneStatus airplaneStatus) {
        LandingEntity landingEntity = prepareLandingEntity(airplaneStatus);
        databaseService.commitToDatabaseWithTransaction(landingEntity);
    }

    private LandingEntity prepareLandingEntity(AirplaneStatus airplaneStatus) {
        LandingEntity landingEntity = new LandingEntity();
        AirplaneEntity airplaneEntity = databaseService.findAirplaneByName(airplaneStatus.getName());
        landingEntity.setLandingEntity(LocalDateTime.now(), airplaneEntity);
        return landingEntity;
    }

    private void addCrashToDatabase(AirplaneStatus airplaneStatus) {
        CrashEntity crashEntity = prepareCrashEntity(airplaneStatus);
        databaseService.commitToDatabaseWithTransaction(crashEntity);
    }

    private CrashEntity prepareCrashEntity(AirplaneStatus airplaneStatus) {
        CrashEntity crashEntity = new CrashEntity();
        AirplaneEntity airplaneEntity = databaseService.findAirplaneByName(airplaneStatus.getName());
        crashEntity.setCrashEntity(LocalDateTime.now(), airplaneEntity);
        return crashEntity;
    }
}