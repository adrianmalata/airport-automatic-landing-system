package database;

import airplane.Airplane;
import database.entities.AirplaneEntity;
import jakarta.persistence.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static jakarta.persistence.Persistence.createEntityManagerFactory;

public class DatabaseService implements AutoCloseable {
    private final Logger logger = LogManager.getLogger(Airplane.class);
    private final EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;
    private static DatabaseService instance;

    private DatabaseService(String persistenceUnitName) {
        entityManagerFactory = createEntityManagerFactory(persistenceUnitName);
        open();
    }

    public static synchronized DatabaseService getInstance() {
        if (instance == null) {
            instance = new DatabaseService("landing-system-database-pu");
        }
        return instance;
    }

    public void cleanAllTables() {
        cleanTable("GreetingAndRejectionEntity.deleteAll");
        cleanTable("CrashEntity.deleteAll");
        cleanTable("LandingEntity.deleteAll");
        cleanTable("AirplaneEntity.deleteAll");
    }

    public void cleanTable(String namedQuery) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createNamedQuery(namedQuery);
        query.executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public void close() {
        if (entityManager != null) {
            entityManager.close();
        }
        if (entityManagerFactory != null) {
            entityManagerFactory.close();
        }
    }

    public synchronized void commitToDatabaseWithTransaction(Object entity) {
        checkIfEntity(entity);
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
    }

    public synchronized  <T> List<T> getRecordsFromTable(String namedQuery, Class<T> entityClass) {
        entityManager.getTransaction().begin();
        TypedQuery<T> query = entityManager.createNamedQuery(namedQuery, entityClass);
        List<T> records = query.getResultList();
        entityManager.getTransaction().commit();
        return records;
    }

    public synchronized AirplaneEntity findAirplaneByName(String name) {
        TypedQuery<AirplaneEntity> query = entityManager.createNamedQuery("AirplaneEntity.findByName", AirplaneEntity.class);
        query.setParameter("name", name);
        List<AirplaneEntity> results = query.getResultList();

        if (!results.isEmpty()) {
            return results.get(0);
        }

        return null;
    }

    private void open() {
        entityManager = entityManagerFactory.createEntityManager();
    }

    private void checkIfEntity(Object entity) {
        if(entity.getClass().getAnnotation(Entity.class) == null) {
            logger.error("Object {} is not marked as @Entity", entity.getClass().getSimpleName());
            throw new IllegalArgumentException("Object is not marked as @Entity");
        }
    }
}
