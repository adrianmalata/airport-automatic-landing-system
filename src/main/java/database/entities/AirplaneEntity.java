package database.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "airplanes")
@NamedQueries({
        @NamedQuery(
                name = "AirplaneEntity.findAll",
                query = "SELECT a FROM AirplaneEntity a"
        ),
        @NamedQuery(
                name = "AirplaneEntity.deleteAll",
                query = "DELETE FROM AirplaneEntity"
        ),
        @NamedQuery(
                name = "AirplaneEntity.findByName",
                query = "SELECT a FROM AirplaneEntity a WHERE a.name = :name"
        )
})

public class AirplaneEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    @OneToOne(mappedBy = "airplaneEntity", cascade = CascadeType.ALL)
    private GreetingAndRejectionEntity greetingAndRejectionEntity;

    @OneToOne(mappedBy = "airplaneEntity", cascade = CascadeType.ALL)
    private CrashEntity crashEntity;

    @OneToOne(mappedBy = "airplaneEntity", cascade = CascadeType.ALL)
    private LandingEntity landingEntity;

    public void setAirplaneEntity(String name, GreetingAndRejectionEntity greetingAndRejectionEntity, CrashEntity crashEntity, LandingEntity landingEntity) {
        this.name = name;
        this.greetingAndRejectionEntity = greetingAndRejectionEntity;
        this.crashEntity = crashEntity;
        this.landingEntity = landingEntity;
    }
}
