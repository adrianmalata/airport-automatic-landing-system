package database.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "landings")
@NamedQueries({
        @NamedQuery(
                name = "LandingEntity.findAll",
                query = "SELECT l FROM LandingEntity l"
        ),
        @NamedQuery(
                name = "LandingEntity.deleteAll",
                query = "DELETE FROM LandingEntity"
        )
})
public class LandingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "event_dt")
    private LocalDateTime eventDt;

    @OneToOne
    @JoinColumn(name = "airplane_id")
    private AirplaneEntity airplaneEntity;

    public void setLandingEntity(LocalDateTime eventDt, AirplaneEntity airplaneEntity) {
        this.eventDt = eventDt;
        this.airplaneEntity = airplaneEntity;
    }
}