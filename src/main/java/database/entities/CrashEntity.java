package database.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "crashes")
@NamedQueries({
        @NamedQuery(
                name = "CrashEntity.findAll",
                query = "SELECT c FROM CrashEntity c"
        ),
        @NamedQuery(
                name = "CrashEntity.deleteAll",
                query = "DELETE FROM CrashEntity"
        )
})
public class CrashEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "event_dt")
    private LocalDateTime eventDt;

    @OneToOne
    @JoinColumn(name = "airplane_id")
    private AirplaneEntity airplaneEntity;

    public void setCrashEntity(LocalDateTime eventDt, AirplaneEntity airplaneEntity) {
        this.eventDt = eventDt;
        this.airplaneEntity = airplaneEntity;
    }
}