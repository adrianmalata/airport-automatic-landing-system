package database.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "greetings_and_rejections")
@NamedQueries({
        @NamedQuery(
                name = "GreetingAndRejectionEntity.findAll",
                query = "SELECT g FROM GreetingAndRejectionEntity g"
        ),
        @NamedQuery(
                name = "GreetingAndRejectionEntity.deleteAll",
                query = "DELETE FROM GreetingAndRejectionEntity"
        )
})
public class GreetingAndRejectionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "event_dt")
    private LocalDateTime eventDt;

    @OneToOne
    @JoinColumn(name = "airplane_id")
    private AirplaneEntity airplaneEntity;

    private Integer port;
    private Boolean greeted;

    public void setGreetingAndRejectionEntity(LocalDateTime eventDt, AirplaneEntity airplaneEntity, Integer port, Boolean greeted) {
        this.eventDt = eventDt;
        this.airplaneEntity = airplaneEntity;
        this.port = port;
        this.greeted = greeted;
    }
}