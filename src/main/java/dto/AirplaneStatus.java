package dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import airplane.utilities.NameGenerator;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class AirplaneStatus {

    // X – width, Y – height, Z – depth, position[X, Y, Z]

    @JsonPropertyOrder({
            "name",
            "currentPosition",
            "destination",
            "vector",
            "velocity",
            "destinationFlag",
            "landedFlag",
            "collisionProcedureOn",
            "fuel",
            "crashed"})
    @EqualsAndHashCode.Include
    protected String name = NameGenerator.generateNewAirplaneName();
    protected Coordinate currentPosition;
    protected Coordinate destination;
    protected List<Double> vector;
    protected double velocity;
    protected boolean destinationFlag;
    protected boolean landedFlag;
    protected boolean crashed;
    protected boolean collisionProcedureOn;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    protected LocalTime fuel;

    public AirplaneStatus() {}

    @JsonCreator
    public AirplaneStatus(
            @JsonProperty("name") String name,
            @JsonProperty("currentPosition") Coordinate currentPosition,
            @JsonProperty("destination") Coordinate destination,
            @JsonProperty("vector") List<Double> vector,
            @JsonProperty("velocity") double velocity,
            @JsonProperty("destinationFlag") boolean destinationFlag,
            @JsonProperty("landedFlag") boolean landedFlag,
            @JsonProperty("collisionProcedureOn") boolean collisionProcedureOn,
            @JsonProperty("fuel") LocalTime fuel,
            @JsonProperty("crashed") boolean crashed) {
        this.name = name;
        this.currentPosition = currentPosition;
        this.destination = destination;
        this.vector = vector;
        this.velocity = velocity;
        this.destinationFlag = destinationFlag;
        this.landedFlag = landedFlag;
        this.collisionProcedureOn = collisionProcedureOn;
        this.fuel = fuel;
        this.crashed = crashed;
    }

    @Override
    public String toString() {
        return String.format(
                "{\"name\":\"%s\",\"currentPosition\":%s,\"destination\":%s,\"vector\":%s,\"velocity\":%s,\"destinationFlag\":%s,\"landedFlag\":%s,\"collisionProcedureOn\":%s,\"fuel\":%s,\"crashed\":%s}",
                name, currentPosition, destination, prepareListToString(vector), velocity, destinationFlag, landedFlag, collisionProcedureOn, prepareLocalTimeToString(fuel), crashed);
    }

    public static String prepareListToString(List<Double> list) {
        return list.toString().replace(" ", "");
    }

    private String prepareLocalTimeToString(LocalTime localTime) {
        return "\"" + localTime.format(DateTimeFormatter.ofPattern("HH:mm:ss")) + "\"";
    }
}