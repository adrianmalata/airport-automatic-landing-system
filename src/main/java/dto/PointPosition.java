package dto;

public record PointPosition(String name, Coordinate currentPosition, boolean landedFlag, boolean crashed) {}