package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Runway extends Space {
    private final FlightPattern landingPattern;
    private final double landingApproachVelocity;
    private final double landingTouchdownVelocity;
    private final String name;
    private boolean occupied;
    private AirplaneStatus occupyingAirplane;

    public Runway(String name, double width, double height, double depth, FlightPattern landingPattern) {
        super(width, height, depth);
        this.name = name;
        this.landingPattern = landingPattern;
        this.landingApproachVelocity = 0.07196; //0.07196
        this.landingTouchdownVelocity = 0.06168; //0.06168
    }
}