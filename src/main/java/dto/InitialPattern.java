package dto;

import lombok.Getter;

@Getter
public class InitialPattern {
    private final FlightPattern topLeft;
    private final FlightPattern topRight;
    private final FlightPattern bottomLeft;
    private final FlightPattern bottomRight;

    public InitialPattern() {
        this.topLeft = new FlightPattern(new Coordinate(5000, Double.NaN, 9000), new Coordinate(5000, Double.NaN, 7000));
        this.topRight = new FlightPattern(new Coordinate(5000, Double.NaN, 1000), new Coordinate(1000, Double.NaN, 5000), new Coordinate(5000, Double.NaN, 7500));
        this.bottomLeft = new FlightPattern(new Coordinate(5000, Double.NaN, 7400));
        this.bottomRight = new FlightPattern(new Coordinate(1100, Double.NaN, 5000), new Coordinate(5000, Double.NaN, 7600));
    }
}