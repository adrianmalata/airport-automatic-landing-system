package dto;

public record Coordinate(double x, double y, double z) {

    @Override
    public String toString() {
        return String.format("{\"x\":%s,\"y\":%s,\"z\":%s}", x, y, z);
    }
}