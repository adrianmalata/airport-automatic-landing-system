package dto;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public class FlightPattern {
    private final List<Coordinate> list;

    public FlightPattern(Coordinate... pattern) {
        this.list = new ArrayList<>();
        this.list.addAll(Arrays.asList(pattern));
    }
}