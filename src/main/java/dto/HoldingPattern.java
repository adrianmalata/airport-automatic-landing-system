package dto;

import lombok.Getter;

import java.util.List;

@Getter
public class HoldingPattern {
    // Holding patterns XZ: left top HOLDING FIX and clockwise.

    private final Coordinate holdingFix;
    private final FlightPattern below4300;
    private final FlightPattern above4300;
    private final List<Double> layers;
    private final double velocityBelow4300;
    private final double velocityAbove4300;

    public HoldingPattern() {
        this.holdingFix = new Coordinate(9200, Double.NaN,  5705.45);
        this.below4300 = new FlightPattern(holdingFix, new Coordinate(9200, Double.NaN, 4194.55), new Coordinate(2033.33, Double.NaN, 4194.55), new Coordinate(2033.33, Double.NaN, 5705.45));
        this.above4300 = new FlightPattern(holdingFix, new Coordinate(9200, Double.NaN, 4294.55), new Coordinate(1866.67, Double.NaN, 4294.55), new Coordinate(1866.67, Double.NaN, 5705.45));
        this.layers = List.of(5000.0, 4700.0, 4400.0, 4100.0, 3800.0, 3500.0, 3200.0, 2900.0, 2600.0, 2300.0);
        this.velocityBelow4300 = 0.11944; //0.11944
        this.velocityAbove4300 = 0.12222; //0.12222
    }
}
