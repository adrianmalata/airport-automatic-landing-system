package dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Command {

    @JsonPropertyOrder({
            "name",
            "currentPosition",
            "destination",
            "velocity",
            "collisionProcedureOn",
            "crashed"})
    @EqualsAndHashCode.Include
    private String name;
    private Coordinate currentPosition;
    private Coordinate destination;
    private double velocity;
    private boolean collisionProcedureOn;
    private boolean crashed;

    public Command() {}

    @JsonCreator
    public Command(
            @JsonProperty("name") String name,
            @JsonProperty("currentPosition") Coordinate currentPosition,
            @JsonProperty("destination") Coordinate destination,
            @JsonProperty("velocity") double velocity,
            @JsonProperty("collisionProcedureOn") boolean collisionProcedureOn,
            @JsonProperty("crashed") boolean crashed) {
        this.name = name;
        this.currentPosition = currentPosition;
        this.destination = destination;
        this.velocity = velocity;
        this.collisionProcedureOn = collisionProcedureOn;
        this.crashed = crashed;
    }

    @Override
    public String toString() {
        return String.format("{\"name\":\"%s\",\"currentPosition\":%s,\"destination\":%s,\"velocity\":%s,\"collisionProcedureOn\":%s,\"crashed\":%s}",
                name, currentPosition, destination, velocity, collisionProcedureOn, crashed);
    }
}