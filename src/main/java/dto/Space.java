package dto;

import lombok.Getter;

@Getter
public class Space {
    private final double width;
    private final double height;
    private final double depth;

    public Space(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
}