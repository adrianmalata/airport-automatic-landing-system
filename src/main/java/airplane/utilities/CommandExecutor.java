package airplane.utilities;

import dto.AirplaneStatus;
import dto.FlightPattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Command;
import dto.Coordinate;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static airport.AirportData.*;
import static airport.AirportSystem.calculateDistanceBetweenCoordinates;

public class CommandExecutor {
    private final Logger logger = LogManager.getLogger(CommandExecutor.class);

    private final AirplaneStatus airplaneStatus;

    public CommandExecutor(AirplaneStatus airplaneStatus) {
        this.airplaneStatus = airplaneStatus;
    }

    public void executeCommand(Command command) {
        handleParsedCommand(command);
        flyStraightWithVector();
        setDestinationFlagNearDestination();
        setLandedFlagIfLanded();
        setCollisionProcedure(command);
        handleFuel();
        reportCrash(command);
    }

    public List<Double> calculateVectorWithDestination(Coordinate destination) {
        Coordinate currentPosition = airplaneStatus.getCurrentPosition();
        double velocity = airplaneStatus.getVelocity();

        double vectorX = destination.x() - currentPosition.x();
        double vectorY = destination.y() - currentPosition.y();
        double vectorZ = destination.z() - currentPosition.z();

        double distance = calculateDistanceBetweenCoordinates(airplaneStatus.getCurrentPosition(), destination);

        vectorX /= distance;
        vectorY /= distance;
        vectorZ /= distance;

        vectorX *= velocity;
        vectorY *= velocity;
        vectorZ *= velocity;

        return List.of(vectorX, vectorY, vectorZ);
    }

    private void handleParsedCommand(Command command) {
        if (command.getDestination() != null) {
            airplaneStatus.setDestination(command.getDestination());
        }
        if (!Double.isNaN(command.getVelocity())) {
            airplaneStatus.setVelocity(command.getVelocity());
        }
        airplaneStatus.setVector(calculateVectorWithDestination(airplaneStatus.getDestination()));
    }

    private void flyStraightWithVector() {
        Coordinate currentPosition = airplaneStatus.getCurrentPosition();
        List<Double> vector = airplaneStatus.getVector();
        double x = currentPosition.x() + vector.get(0);
        double y = currentPosition.y() + vector.get(1);
        double z = currentPosition.z() + vector.get(2);
        airplaneStatus.setCurrentPosition(new Coordinate(x, y, z));
    }

    private void setDestinationFlagNearDestination() {
        Coordinate destination = airplaneStatus.getDestination();
        double distance = calculateDistanceBetweenCoordinates(airplaneStatus.getCurrentPosition(), destination);

        airplaneStatus.setDestinationFlag(distance <= NEAR_DESTINATION_DISTANCE);
    }

    private void setLandedFlagIfLanded() {
        Coordinate destination = airplaneStatus.getDestination();

        Coordinate lastLandingPointRunway01 = getLastLandingPoint(getLandingPattern(1));
        Coordinate lastLandingPointRunway02 = getLastLandingPoint(getLandingPattern(2));

        if(destination.equals(lastLandingPointRunway01) || destination.equals(lastLandingPointRunway02)) {
            setLandedFlagIfNearDestination(destination);
        }
    }

    private Coordinate getLastLandingPoint(FlightPattern landingPattern) {
        List<Coordinate> runwayLandingPattern = landingPattern.getList();
        return runwayLandingPattern.get(runwayLandingPattern.size() - 1);
    }

    private void setLandedFlagIfNearDestination(Coordinate destination) {
        double distanceFromDestination = calculateDistanceBetweenCoordinates(airplaneStatus.getCurrentPosition(), destination);
        if (distanceFromDestination <= NEAR_DESTINATION_DISTANCE) {
            airplaneStatus.setLandedFlag(true);
        }
    }

    private void setCollisionProcedure(Command command) {
        boolean commandContainsTrue = Boolean.TRUE.equals(command.isCollisionProcedureOn());
        boolean systemHasCollisionProcedureOn = airplaneStatus.isCollisionProcedureOn();

        if (commandContainsTrue && !systemHasCollisionProcedureOn) {
            airplaneStatus.setCollisionProcedureOn(true);
        } else {
            airplaneStatus.setCollisionProcedureOn(commandContainsTrue);
        }
    }

    private void handleFuel() {
        updateFuel();
        checkFuelStatus();
    }

    private void checkFuelStatus() {
        boolean outOfFuel = airplaneStatus.getFuel().equals(LocalTime.of(0, 0));

        if (outOfFuel) {
            logger.info("Airplane {} is out of fuel", airplaneStatus.getName());
            airplaneStatus.setCrashed(true);
        }
    }

    private void updateFuel() {
        LocalTime fuel = airplaneStatus.getFuel();
        fuel = fuel.minus(1, ChronoUnit.MILLIS);
        airplaneStatus.setFuel(fuel);
    }

    private void reportCrash(Command command) {
        if (command.isCrashed()) {
            airplaneStatus.setCrashed(true);
        }
    }
}