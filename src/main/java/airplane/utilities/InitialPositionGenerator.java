package airplane.utilities;

import dto.Coordinate;

import java.util.Random;
import java.util.random.RandomGenerator;

import static airport.AirportData.AIRSPACE;

public class InitialPositionGenerator {
    private static final Random RANDOM = Random.from(RandomGenerator.getDefault());

    private InitialPositionGenerator() {
    }

    public static Coordinate appearOnRadar() {
        double coordinateY = generateRandomCoordinate(AIRSPACE.getHeight() - 1000, AIRSPACE.getHeight() + 1);

        double randomX = generateRandomCoordinate(0, AIRSPACE.getWidth() + 1);
        double randomZ = generateRandomCoordinate(0, AIRSPACE.getDepth() + 1);
        double[] coordinatesXZ = decideOnWhichBorderAppear(randomX, randomZ);

        return new Coordinate(coordinatesXZ[0], coordinateY, coordinatesXZ[1]);
    }

    private static double generateRandomCoordinate(double randomOrigin, double randomBound) {
        return RANDOM.nextDouble(randomOrigin, randomBound);
    }

    private static double[] decideOnWhichBorderAppear(double randomX, double randomZ) {
        double coordinateX;
        double coordinateZ;

        if (randomX > randomZ) {
            coordinateX = randomX;
            coordinateZ = (RANDOM.nextDouble() < 0.5) ? 0 : AIRSPACE.getDepth();
        } else {
            coordinateX = (RANDOM.nextDouble() < 0.5) ? 0 : AIRSPACE.getWidth();
            coordinateZ = randomZ;
        }

        return new double[]{coordinateX, coordinateZ};
    }
}