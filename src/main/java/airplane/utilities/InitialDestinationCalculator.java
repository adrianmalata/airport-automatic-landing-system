package airplane.utilities;

import dto.Coordinate;

import static airport.AirportData.INITIAL_PATTERN;

public class InitialDestinationCalculator {

    private InitialDestinationCalculator() {
    }

    public static Coordinate calculateInitialDestination(Coordinate currentPosition) {
        boolean isInQ1 = currentPosition.x() >= 5000 && currentPosition.z() >= 5000;
        boolean isInQ2 = currentPosition.x() >= 5000 && currentPosition.z() < 5000;
        boolean isInQ3 = currentPosition.x() < 5000 && currentPosition.z() >= 5000;
        Coordinate initialPatternCoordinate = getInitialPatternCoordinate(isInQ1, isInQ2, isInQ3);
        return new Coordinate(initialPatternCoordinate.x(), currentPosition.y(), initialPatternCoordinate.z());
    }

    private static Coordinate getInitialPatternCoordinate(boolean isInQ1, boolean isInQ2, boolean isInQ3) {
        Coordinate initialPatternCoordinate;
        if (isInQ1) {
            initialPatternCoordinate = INITIAL_PATTERN.getTopLeft().getList().get(0);
        } else if (isInQ2) {
            initialPatternCoordinate = INITIAL_PATTERN.getTopRight().getList().get(0);
        } else if (isInQ3) {
            initialPatternCoordinate = INITIAL_PATTERN.getBottomLeft().getList().get(0);
        } else {
            initialPatternCoordinate = INITIAL_PATTERN.getBottomRight().getList().get(0);
        }
        return initialPatternCoordinate;
    }
}