package airplane.utilities;

import dto.AirplaneStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class MessageManager {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());
    private static final Logger LOGGER = LogManager.getLogger(MessageManager.class);
    private final AirplaneStatus airplaneStatus;

    public MessageManager(AirplaneStatus airplaneSystem) {
        airplaneStatus = airplaneSystem;
    }

    public String prepareInitialCall() {
        return getAirplaneStatusString();
    }

    public String getAirplaneStatusString() {
        ObjectNode status = OBJECT_MAPPER.createObjectNode();
        String fuel = airplaneStatus.getFuel().format(DateTimeFormatter.ofPattern("HH:mm:ss"));

        status.set("name", TextNode.valueOf(airplaneStatus.getName()));
        status.set("currentPosition", OBJECT_MAPPER.valueToTree(airplaneStatus.getCurrentPosition()));
        status.set("destination", OBJECT_MAPPER.valueToTree(airplaneStatus.getDestination()));
        status.set("vector", listToJsonNode(airplaneStatus.getVector()));
        status.set("velocity", DoubleNode.valueOf(airplaneStatus.getVelocity()));
        status.set("destinationFlag", BooleanNode.valueOf(airplaneStatus.isDestinationFlag()));
        status.set("landedFlag", BooleanNode.valueOf(airplaneStatus.isLandedFlag()));
        status.set("collisionProcedureOn", BooleanNode.valueOf(airplaneStatus.isCollisionProcedureOn()));
        status.set("fuel", TextNode.valueOf(fuel));
        status.set("crashed", BooleanNode.valueOf(airplaneStatus.isCrashed()));

        return status.toString();
    }

    public static AirplaneStatus parseAirplaneStatus(String status) {
        try {
            return OBJECT_MAPPER.readValue(status, AirplaneStatus.class);
        } catch (JsonProcessingException notStatus) {
            LOGGER.error("JsonProcessingException: Tried to parse airplane status from JSON for status {} but this might not be a status", status);
            throw new IllegalStateException();
        }
    }

    private ArrayNode listToJsonNode(List<Double> values) {
        ArrayNode node = OBJECT_MAPPER.createArrayNode();
        for (double value : values) {
            node.add(value);
        }
        return node;
    }
}