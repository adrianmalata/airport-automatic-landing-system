package airplane.utilities;

import lombok.Getter;

import java.util.List;
import java.util.Random;
import java.util.random.RandomGenerator;

public class NameGenerator {
    private static final Random RANDOM = Random.from(RandomGenerator.getDefault());

    @Getter
    private static final List<String> PREFIXES = List.of(
            "AFR", "BAW", "DLH", "EZY", "FDB", "KLM", "LOT",
            "QTR", "RYR", "RYS", "SAS", "SWR", "THY", "WZZ");

    private NameGenerator() {
    }

    public static String generateNewAirplaneName() {
        StringBuilder name = new StringBuilder();
        generateRandomPrefix(name);
        generateRandomSuffix(name);
        return name.toString();
    }

    private static void generateRandomPrefix(StringBuilder name) {
        name.append(PREFIXES.get(RANDOM.nextInt(PREFIXES.size())));
    }

    private static void generateRandomSuffix(StringBuilder name) {
        for (int i = 0; i < 4; i++) {
            name.append(RANDOM.nextInt(0, 9));
        }
    }
}