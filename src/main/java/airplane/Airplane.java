package airplane;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static utilities.PropertiesUtilities.CLIENT_IP;

@Getter
public class Airplane implements Runnable {
    private final Logger logger = LogManager.getLogger(Airplane.class);
    private final AirplaneClient airplaneClient;
    private final int localPort;

    public Airplane(int localPort) {
        this.localPort = localPort;
        airplaneClient = new AirplaneClient();
    }

    @Override
    public void run() {
        connectToServer();
        airplaneClient.handleCommunication();
    }

    private void connectToServer() {
        airplaneClient.connectToServer(CLIENT_IP, localPort);
        logger.info("Airplane {} communication working on local port {} (port {})", airplaneClient.getAirplaneSystem().getName(), airplaneClient.getCommunication().getSocket().getLocalPort(), airplaneClient.getCommunication().getSocket().getPort());
    }
}