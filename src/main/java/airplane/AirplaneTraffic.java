package airplane;

import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import networking.Client;
import database.DatabaseService;
import networking.IOCommunication;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.random.RandomGenerator;

import static utilities.PropertiesUtilities.*;

public class AirplaneTraffic extends Client {
    private static final Logger LOGGER = LogManager.getLogger(AirplaneTraffic.class);
    private static final AtomicInteger airplaneCount = new AtomicInteger(0);
    private static IOCommunication mainCommunication;

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(5000);
        createMainCommunicationChannel();

        try (DatabaseService databaseService = DatabaseService.getInstance();
             ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_AIRPLANES)
        ) {
            databaseService.cleanAllTables();

            while (true) {
                simulateRandomTimeAppearing();
                int localPort = AIRPLANE_START_PORT + airplaneCount.get();
                mainCommunication.sendMessage(localPort);
                executorService.execute(new Airplane(localPort));
                airplaneCount.incrementAndGet();
            }
        }
    }

    private static void createMainCommunicationChannel() {
        Socket socket = createSocketWithRetry(CLIENT_IP, MAIN_COMMUNICATION_PORT);
        mainCommunication = createCommunicationWithRetry(socket);
    }

    private static void simulateRandomTimeAppearing() {
        try {
            long randomDelay = Random.from(RandomGenerator.getDefault()).nextLong(AirplaneSystem.MIN_APPEARING_DELAY, AirplaneSystem.MAX_APPEARING_DELAY);
            Thread.sleep(randomDelay);
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException: Current thread {} interrupted", Thread.currentThread().getName());
            Thread.currentThread().interrupt();
        }
    }
}