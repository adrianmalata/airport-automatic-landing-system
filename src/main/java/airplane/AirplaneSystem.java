package airplane;

import airplane.utilities.CommandExecutor;
import airplane.utilities.InitialDestinationCalculator;
import airplane.utilities.InitialPositionGenerator;
import dto.AirplaneStatus;
import lombok.EqualsAndHashCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Coordinate;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class AirplaneSystem extends AirplaneStatus {
    public static final double INITIAL_VELOCITY = 0.23611; //0.23611
    public static final long MIN_APPEARING_DELAY = 60000; //60000
    public static final long MAX_APPEARING_DELAY = 120000; //120000
    private final Logger logger = LogManager.getLogger(AirplaneSystem.class);

    public AirplaneSystem() {
        Coordinate initialPosition = InitialPositionGenerator.appearOnRadar();
        setCurrentPosition(initialPosition);

        Coordinate initialDestination = InitialDestinationCalculator.calculateInitialDestination(getCurrentPosition());
        setDestination(initialDestination);

        setVelocity(INITIAL_VELOCITY);

        CommandExecutor commandExecutor = new CommandExecutor(this);
        List<Double> initialVector = commandExecutor.calculateVectorWithDestination(getDestination());
        setVector(initialVector);

        setLandedFlag(false);

        setCollisionProcedureOn(false);

        setFuel(LocalTime.of(3, 0));
    }

    public Map.Entry<Boolean, LocalDateTime> turnAroundIfNecessary(String initialResponse) {
        if (initialResponse.contains("Sorry " + getName())) {
            logger.info("Airplane {} rejected and turning around", getName());
            return Map.entry(false, LocalDateTime.now());
        } else {
            logger.info("Airplane {} greeted and flying to the top holding fix", getName());
            return Map.entry(true, LocalDateTime.now());
        }
    }

    public Coordinate getNextPosition() {
        Coordinate position = getCurrentPosition();
        List<Double> currentVector = getVector();

        double nextX = position.x() + currentVector.get(0);
        double nextY = position.y() + currentVector.get(1);
        double nextZ = position.z() + currentVector.get(2);

        return new Coordinate(nextX, nextY, nextZ);
    }

    public List<Coordinate> getOneSecondNextPositions() {
        List<Coordinate> oneSecondNextPositions = new ArrayList<>();
        Coordinate position = getCurrentPosition();
        List<Double> currentVector = getVector();

        for (int i=1; i <= 100; i++) {
            double nextX = position.x() + i * currentVector.get(0);
            double nextY = position.y() + i * currentVector.get(1);
            double nextZ = position.z() + i * currentVector.get(2);
            oneSecondNextPositions.add(new Coordinate(nextX, nextY, nextZ));
        }

        return oneSecondNextPositions;
    }

    public static AirplaneSystem getAirplaneSystemFromAirplaneStatus(AirplaneStatus airplaneStatus) {
        AirplaneSystem airplaneSystem = new AirplaneSystem();
        airplaneSystem.setName(airplaneStatus.getName());
        airplaneSystem.setCurrentPosition(airplaneStatus.getCurrentPosition());
        airplaneSystem.setDestination(airplaneStatus.getDestination());
        airplaneSystem.setVector(airplaneStatus.getVector());
        airplaneSystem.setVelocity(airplaneStatus.getVelocity());
        airplaneSystem.setDestinationFlag(airplaneStatus.isDestinationFlag());
        airplaneSystem.setLandedFlag(airplaneStatus.isLandedFlag());
        airplaneSystem.setCollisionProcedureOn(airplaneStatus.isCollisionProcedureOn());
        airplaneSystem.setFuel(airplaneStatus.getFuel());
        airplaneSystem.setCrashed(airplaneStatus.isCrashed());
        return airplaneSystem;
    }
}