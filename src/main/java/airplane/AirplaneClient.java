package airplane;

import airplane.utilities.CommandExecutor;
import airplane.utilities.MessageManager;
import database.entities.AirplaneEntity;
import database.entities.GreetingAndRejectionEntity;
import lombok.Getter;
import networking.Client;
import dto.Command;
import database.DatabaseService;

import java.time.LocalDateTime;
import java.util.Map;

public class AirplaneClient extends Client {
    @Getter
    private final AirplaneSystem airplaneSystem;
    private final MessageManager messageManager;
    private final CommandExecutor commandExecutor;
    private final DatabaseService databaseService;
    private Map.Entry<Boolean, LocalDateTime> airplaneInvited;


    public AirplaneClient() {
        airplaneSystem = new AirplaneSystem();
        databaseService = DatabaseService.getInstance();
        messageManager = new MessageManager(airplaneSystem);
        commandExecutor = new CommandExecutor(airplaneSystem);
    }

    public void handleCommunication() {
        sendInitialCall();
        readAndExecuteInitialCommand();
        addAirplaneToDatabase();

        while (!communication.isClosed()) {
            sendStatus();
            readAndExecuteCommand();
        }
    }

    private void sendInitialCall() {
        String initialCall = messageManager.prepareInitialCall();
        communication.sendMessage(initialCall);
    }

    private void readAndExecuteInitialCommand() {
        String initialCallFromAirport = communication.readLine();
        airplaneInvited = airplaneSystem.turnAroundIfNecessary(initialCallFromAirport);

        if (airplaneInvited.getKey()) {
            Command initialCommand = airport.utilities.MessageManager.parseCommand(initialCallFromAirport);
            commandExecutor.executeCommand(initialCommand);
        } else {
            communication.closeCommunication();
        }
    }

    private void sendStatus() {
        String status = messageManager.getAirplaneStatusString();
        communication.sendMessage(status);
    }

    private void readAndExecuteCommand() {
        Command commandFromAirport = readAndParseCommand();

        if (commandFromAirport.getVelocity() == -1) {
            communication.closeCommunication();
        } else {
            commandExecutor.executeCommand(commandFromAirport);
        }
    }

    private Command readAndParseCommand() {
        String commandFromAirport = communication.readLine();
        return airport.utilities.MessageManager.parseCommand(commandFromAirport);
    }

    private void addAirplaneToDatabase() {
        AirplaneEntity airplaneEntity = prepareAirplaneEntity();
        databaseService.commitToDatabaseWithTransaction(airplaneEntity);
    }

    private AirplaneEntity prepareAirplaneEntity() {
        AirplaneEntity airplaneEntity = new AirplaneEntity();
        String name = airplaneSystem.getName();
        GreetingAndRejectionEntity greetingAndRejectionEntity = prepareGreetingAndRejection(airplaneEntity);

        airplaneEntity.setName(name);
        airplaneEntity.setGreetingAndRejectionEntity(greetingAndRejectionEntity);
        return airplaneEntity;
    }

    private GreetingAndRejectionEntity prepareGreetingAndRejection(AirplaneEntity airplaneEntity) {
        GreetingAndRejectionEntity greetingAndRejectionEntity = new GreetingAndRejectionEntity();
        LocalDateTime eventDt = airplaneInvited.getValue();
        int port = clientSocket.getPort();
        Boolean greeted = airplaneInvited.getKey();

        greetingAndRejectionEntity.setGreetingAndRejectionEntity(eventDt, airplaneEntity, port, greeted);
        return greetingAndRejectionEntity;
    }
}