package simulation.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import dto.Coordinate;
import dto.PointPosition;

public class MessageManager {

    private static final Logger LOGGER = LogManager.getLogger(MessageManager.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private MessageManager() {
    }

    public static String getPointPositionString(String name, Coordinate position, boolean landedFlag, boolean crashed) {
        ObjectNode preparedCommand = OBJECT_MAPPER.createObjectNode();

        preparedCommand.set("name", TextNode.valueOf(name));
        preparedCommand.set("currentPosition", OBJECT_MAPPER.valueToTree(position));
        preparedCommand.set("landedFlag", BooleanNode.valueOf(landedFlag));
        preparedCommand.set("crashed", BooleanNode.valueOf(crashed));

        return preparedCommand.toString();
    }

    public static PointPosition parsePointMessage(String status) {
        try {
            return OBJECT_MAPPER.readValue(status, PointPosition.class);
        } catch (JsonProcessingException notPointPosition) {
            LOGGER.error("JsonProcessingException: Tried to parse point message from JSON for status {} but this might not be a status", status);
            throw new IllegalStateException();
        }
    }

}