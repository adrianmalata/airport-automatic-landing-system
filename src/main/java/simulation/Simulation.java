package simulation;

import javafx.scene.shape.Sphere;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simulation.utilities.MessageManager;
import networking.Client;
import javafx.application.Platform;
import networking.IOCommunication;
import dto.PointPosition;
import javafx.application.Application;
import javafx.stage.Stage;
import visualisation.utilities.Point;
import visualisation.Visualisation;
import visualisation.utilities.PointMaker;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static utilities.PropertiesUtilities.*;

public class Simulation extends Application {
    private static final Logger LOGGER = LogManager.getLogger(Simulation.class);
    private final Visualisation visualisation = new Visualisation();
    private final Client client = new Client();
    private final List<Point> points = new CopyOnWriteArrayList<>();
    private IOCommunication communication;

    @Override
    public void start(Stage stage) {
        client.connectToServer(CLIENT_IP, SIMULATION_PORT);
        communication = client.getCommunication();

        Thread clientThread = getClientThread();

        visualisation.start(stage);
        stage.show();

        stage.setOnCloseRequest(close -> clientThread.interrupt());
    }

    private Thread getClientThread() {
        Runnable clientTask = this::handleClientCommunication;

        Thread clientThread = new Thread(clientTask);
        clientThread.start();

        return clientThread;
    }

    private void handleClientCommunication() {
        boolean areAnyMoreSpheres = visualisation.getGroup().getChildren().stream().anyMatch(Sphere.class::isInstance);

        while (!communication.isClosed() && !areAnyMoreSpheres) {
            try {
                handleServerMessage();
            } catch (IllegalArgumentException illegalArgumentException) {
                LOGGER.info("AirplaneEntity disconnected, closing simulation connection on local port {} (port {})", communication.getSocket().getLocalPort(), communication.getSocket().getPort());
                communication.closeCommunication();
            } catch (Exception exception) {
                LOGGER.error("Exception: {} occurred, message: {}", exception.getClass().getName(), exception.getMessage());
            }
        }
        client.stopClient();
    }

    private void handleServerMessage() {
        String messageFromServer = communication.readLine();
        PointPosition pointPosition = MessageManager.parsePointMessage(messageFromServer);
        boolean pointsContainPointPositionName = isPointsContainsPointPositionName(pointPosition);

        if (pointsContainPointPositionName) {
            updateCurrentPoints(pointPosition);
        } else {
            Point newPoint = new Point(visualisation, pointPosition.name());
            PointMaker.makePoint(newPoint, pointPosition.currentPosition());
            Platform.runLater(() -> visualisation.addPointToVisualisationGroup(newPoint));
            points.add(newPoint);
        }
    }

    private boolean isPointsContainsPointPositionName(PointPosition pointPosition) {
        return points.stream()
                .anyMatch(point -> point.getName().equals(pointPosition.name()));
    }

    private void updateCurrentPoints(PointPosition pointPosition) {
        for (Point point : points) {
            if (point.getName().equals(pointPosition.name())) {
                if (pointPosition.landedFlag() || pointPosition.crashed()) {
                    PointMaker.errasePoint(point);
                    Platform.runLater(() -> visualisation.removePointFromVisualisationGroup(point));
                    points.remove(point);
                } else {
                    Platform.runLater(() -> point.updatePointPosition(pointPosition.currentPosition()));
                }
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}