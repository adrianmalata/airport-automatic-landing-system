package networking;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Server {
    private static final Logger LOGGER = LogManager.getLogger(Server.class);
    protected final List<ManagedServerSocket> airplaneServerSockets = new CopyOnWriteArrayList<>();
    protected final List<Socket> airplaneClientSockets = new CopyOnWriteArrayList<>();
    protected final List<IOCommunication> airplaneCommunications = new CopyOnWriteArrayList<>();
    protected ServerSocket simulationServerSocket;
    protected Socket simulationClientSocket;
    protected IOCommunication simulationCommunication;
    private int mainCommunicationPort;
    private IOCommunication mainCommunication;

    public void runServer(int mainCommunicationPort, int simulationLocalPort) {
        this.mainCommunicationPort = mainCommunicationPort;
        createMainCommunication();
        handleServerSockets();
        simulationServerSocket = createServerSocket(simulationLocalPort);
        LOGGER.info("Server running");
    }

    public void connectAirplanes() {
        new Thread(() -> {
            while (true) {
                handleClientSockets();
                handleCommunications();
            }
        }).start();
    }

    public void stopServer() {
        closeClientSockets(airplaneClientSockets);
        closeServerSockets(airplaneServerSockets);

        closeClientSocket(simulationClientSocket);
        closeServerSocket(simulationServerSocket);

        closeWholeCommunication();
    }

    private void handleClientSockets() {
        createClientSockets();
        cleanUnusedClientSockets();
    }

    private void createClientSockets() {
        ServerSocket serverSocket = getFromListWithRetry(airplaneServerSockets, "airplaneServerSockets", ManagedServerSocket::isUnused);
        airplaneClientSockets.add(createClientSocket(serverSocket));
    }

    protected Socket createClientSocket(ServerSocket serverSocket) {
        try {
            Socket clientSocket = serverSocket.accept();
            LOGGER.info("Client connected on local port {} (port {})", clientSocket.getLocalPort(), clientSocket.getPort());
            return clientSocket;
        } catch (IOException ioException) {
            LOGGER.error("IOException: I/O error occurred while connecting the client to the server on local port {}", serverSocket.getLocalPort());
        }
        return null;
    }

    private void cleanUnusedClientSockets() {
        boolean removed = airplaneClientSockets.removeIf(Socket::isClosed);
        if (removed) {
            LOGGER.info("Removed unused client sockets, current number of client sockets: {}", airplaneClientSockets.size());
        }
    }

    private void handleCommunications() {
        createCommunications();
        cleanUnusedCommunications();
    }

    private void createCommunications() {
        Socket clientSocket = getFromListWithRetry(airplaneClientSockets, "airplaneClientSockets", Socket::isConnected);
        IOCommunication communication = new AirplaneCommunication(clientSocket);
        LOGGER.info("AirplaneCommunication created on local port {} (port {})", clientSocket.getLocalPort(), clientSocket.getPort());
        airplaneCommunications.add(communication);
    }

    protected IOCommunication createCommunication(Socket clientSocket) {
        IOCommunication communication = new IOCommunication(clientSocket);
        LOGGER.info("IOCommunication created on local port {} (port {})", clientSocket.getLocalPort(), clientSocket.getPort());
        return communication;
    }

    private void cleanUnusedCommunications() {
        boolean removed = airplaneCommunications.removeIf(IOCommunication::isClosed);
        if (removed) {
            LOGGER.info("Removed unused communications, current number of communications: {}", airplaneCommunications.size());
        }
    }

    protected <T> T getFromListWithRetry(List<T> list, String listName, Validator<T> validator) {
        while (true) {
            if (list.isEmpty()) {
                LOGGER.info("List of {} is empty, retrying...", listName);
                retryOneSecond();
                continue;
            }

            T element = list.get(list.size() - 1);
            if (!validator.validate(element)) {
                LOGGER.info("Getting element from {} not worked, retrying...", listName);
                retryOneSecond();
            }else {
                LOGGER.info("Got element: {}", element);
                return element;
            }
        }
    }

    protected void sendMessageWithIOCommunication(IOCommunication communication, String message) {
        communication.sendMessage(message);
    }

    protected String readLineWithIOCommunication(IOCommunication communication) {
        return communication.readLine();
    }

    private void createMainCommunication() {
        ServerSocket serverSocket = createServerSocket(mainCommunicationPort);
        Socket socket = createClientSocket(serverSocket);
        mainCommunication = createCommunication(socket);
    }

    private void handleServerSockets() {
        new Thread(() -> {
            while (true) {
                createServerSockets();
                cleanUnusedServerSockets();
            }
        }).start();
    }

    private void createServerSockets() {
        int localPort = Integer.parseInt(mainCommunication.readLine());
        LOGGER.info("Received new port from main communication: {}", localPort);
        airplaneServerSockets.add(createServerSocket(localPort));
    }

    private void cleanUnusedServerSockets() {
        boolean removed = airplaneServerSockets.removeIf(ManagedServerSocket::isClosed);
        if (removed) {
            LOGGER.info("Removed unused server sockets, current number of server sockets: {}", airplaneServerSockets.size());
        }
    }

    private ManagedServerSocket createServerSocket(int localPort) {
        try {
            ManagedServerSocket managedServerSocket = new ManagedServerSocket(localPort);
            LOGGER.info("Server listening on port {}", localPort);
            return managedServerSocket;
        } catch (IOException ioException) {
            LOGGER.error("IOException: I/O error occurred while opening the server socket on port {}", localPort);
        } catch (IllegalArgumentException illegalArgument) {
            LOGGER.error("IllegalArgumentException: The port {} parameter outside the specified range of valid port values, which is between 0 and 65535", localPort);
        }
        throw new IllegalStateException();
    }

    private void closeClientSockets(List<Socket> clientSockets) {
        for (Socket clientSocket : clientSockets) {
            closeClientSocket(clientSocket);
            clientSockets.remove(clientSocket);
        }
    }

    private void closeClientSocket(Socket clientSocket) {
        try {
            clientSocket.close();
        } catch (IOException ioException) {
            LOGGER.error("IOException: I/O error occurred while closing the client socket on local port {} (port {})", clientSocket.getLocalPort(), clientSocket.getPort());
        }
    }

    private void closeServerSockets(List<ManagedServerSocket> serverSockets) {
        for (ManagedServerSocket serverSocket : serverSockets) {
            closeServerSocket(serverSocket);
            serverSockets.remove(serverSocket);
        }
    }

    private void closeServerSocket(ServerSocket serverSocket) {
        try {
            serverSocket.close();
        } catch (IOException ioException) {
            LOGGER.error("IOException: I/O error occurred while closing the server socket on local port {}", serverSocket.getLocalPort());
        }
    }

    private void closeWholeCommunication() {
        for (IOCommunication airplaneCommunication : airplaneCommunications) {
            airplaneCommunication.closeCommunication();
            airplaneCommunications.remove(airplaneCommunication);
        }
        simulationCommunication.closeCommunication();
        LOGGER.info("Whole communication closed, simulation local port {} (port {}), airplane local ports: {} (ports {})", simulationClientSocket.getLocalPort(), simulationClientSocket.getPort(), airplaneClientSockets.stream().map(Socket::getLocalPort).toList(), airplaneClientSockets.stream().map(Socket::getPort).toList());
    }

    private void retryOneSecond() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException: Current thread {} interrupted", Thread.currentThread().getName());
            Thread.currentThread().interrupt();
        }
    }
}