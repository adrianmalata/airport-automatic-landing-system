package networking;

public interface Validator<T> {
    boolean validate(T element);
}