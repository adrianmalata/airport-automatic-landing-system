package networking;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.Socket;

public class Client {

    private static final Logger LOGGER = LogManager.getLogger(Client.class);
    protected Socket clientSocket;
    @Getter
    protected IOCommunication communication;

    public void connectToServer(String ipAddress, int localPort) {
        clientSocket = createSocketWithRetry(ipAddress, localPort);
        communication = createCommunicationWithRetry(clientSocket);
    }

    public void stopClient() {
        closeClientSocket();
    }

    protected static Socket createSocketWithRetry(String ipAddress, int localPort) {
        while (true) {
            try {
                return new Socket(ipAddress, localPort);
            } catch (IOException ioException) {
                LOGGER.error("IOException: I/O error occurred while creating the socket for local port {}, retrying...", localPort);
                retryOneSecond();
            }
        }
    }

    protected static IOCommunication createCommunicationWithRetry(Socket socket) {
        while (true) {
            try {
                return new IOCommunication(socket);
            } catch (NullPointerException nullPointerException) {
                LOGGER.error("NullPointerException: null error occurred while creating the communication for local port {} (port {}), retrying...", socket.getLocalPort(), socket.getPort());
                retryOneSecond();
            }
        }
    }

    private static void retryOneSecond() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException: Current thread {} interrupted", Thread.currentThread().getName());
            Thread.currentThread().interrupt();
        }
    }

    private void closeClientSocket() {
        try {
            communication.closeCommunication();
            clientSocket.close();
            LOGGER.info("Client socket on local port {} closed (port {})", clientSocket.getLocalPort(), clientSocket.getPort());
        } catch (IOException ioException) {
            LOGGER.error("IOException: I/O error occurred while closing the client socket on local port {} (port {})", clientSocket.getLocalPort(), clientSocket.getPort());
        }
    }
}