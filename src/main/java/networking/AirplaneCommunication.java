package networking;

import lombok.Getter;
import lombok.Setter;

import java.net.Socket;

@Getter
@Setter
public class AirplaneCommunication extends IOCommunication {
    private boolean greeted;

    public AirplaneCommunication(Socket socket) {
        super(socket);
        greeted = false;
    }
}
