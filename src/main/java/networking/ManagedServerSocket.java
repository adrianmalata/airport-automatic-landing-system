package networking;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@Getter
@Setter
public class ManagedServerSocket extends ServerSocket {
    private boolean unused;

    public ManagedServerSocket(int port) throws IOException {
        super(port);
        this.unused = true;
    }

    @Override
    public Socket accept() throws IOException {
        unused = false;
        return super.accept();
    }
}