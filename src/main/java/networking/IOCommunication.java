package networking;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class IOCommunication {

    private static final Logger LOGGER = LogManager.getLogger(IOCommunication.class);
    private PrintWriter output;
    private BufferedReader input;

    @Getter
    private final Socket socket;

    public IOCommunication(Socket socket) {
        this.socket = socket;
        setOutput(socket);
        setInput(socket);
    }

    public void sendMessage(Object message) {
        output.println(message.toString());
    }

    public String readLine() {
        try {
            return input.readLine();
        } catch (IOException ioException) {
            if (isClosed()) {
                LOGGER.error("IOException: I/O communication with client on local port {} closed (port {})", socket.getLocalPort(), socket.getPort());
            } else {
                LOGGER.error("IOException: I/O error occurred while reading command from client on local port {} or communication with client closed (port {})", socket.getLocalPort(), socket.getPort());
            }
        }
        return null;
    }

    public void closeCommunication() {
        try {
            output.close();
            input.close();
            LOGGER.info("Communication on local port {} closed (port {})", socket.getLocalPort(), socket.getPort());
        } catch (IOException ioException) {
            LOGGER.error("IOException: Error occurred while closing the IOCommunication on local port {} (port {})", socket.getLocalPort(), socket.getPort());
        }
    }

    public boolean isClosed() {
        return socket.isClosed();
    }

    private void setOutput(Socket socket) {
        try {
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        } catch (IOException ioException) {
            LOGGER.error("IOException: I/O error occurred while creating the output stream or if the socket wasn't connected on local port {} (port {})", socket.getLocalPort(), socket.getPort());
        }
    }

    private void setInput(Socket socket) {
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ioException) {
            LOGGER.error("IOException: I/O error occurred while creating the input stream or the socket was closed or the socket wasn't connected on local port {} (port {})", socket.getLocalPort(), socket.getPort());
        }
    }

    //this method is used via reflection in Server class
    public boolean isConnected() {
        return socket.isConnected();
    }

    @Override
    public String toString() {
        return "IOCommunication, local port " + socket.getLocalPort() + " (port " + socket.getPort() + ")";
    }
}