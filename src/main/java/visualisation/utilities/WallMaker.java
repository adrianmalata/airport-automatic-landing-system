package visualisation.utilities;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import visualisation.Visualisation;

import static airport.AirportData.AIRSPACE;

public class WallMaker {
    private final Visualisation visualisation;
    private Rectangle wallXZ;
    private Rectangle wallXY;
    private Rectangle wallYZ;

    public WallMaker(Visualisation visualisation) {
        this.visualisation = visualisation;
        makeAllWalls();
        setupAllWalls();
    }

    public void setAllWalls() {
        visualisation.setWallXZ(wallXZ);
        visualisation.setWallXY(wallXY);
        visualisation.setWallYZ(wallYZ);
    }

    public void setupAllWalls() {
        setupWallXZ();
        setupWallXY();
        setupWallYZ();
    }

    private void makeAllWalls() {
        wallXZ = makeWall(AIRSPACE.getWidth() / 10, AIRSPACE.getDepth() / 10);
        wallXY = makeWall(AIRSPACE.getWidth() / 10, AIRSPACE.getHeight() / 10);
        wallYZ = makeWall(AIRSPACE.getDepth() / 10, AIRSPACE.getHeight() / 10);
    }

    private Rectangle makeWall(double width, double height) {
        Rectangle wall = new Rectangle(width, height, Color.LIGHTGRAY);
        addMesh(wall);
        return wall;
    }

    private void addMesh(Rectangle wall) {
        double wallWidth = wall.getWidth();
        double wallHeight = wall.getHeight();
        double boldMeshSize = 50;
        double thinMeshSize = 10;

        Canvas canvas = new Canvas(wallWidth, wallHeight);

        makeMesh(canvas, 0.5, boldMeshSize, wallWidth, wallHeight);
        makeMesh(canvas, 0.2, thinMeshSize, wallWidth, wallHeight);

        Image image = canvas.snapshot(null, null);
        wall.setFill(new ImagePattern(image));
    }

    private void makeMesh(Canvas canvas, double lineWidth, double meshSize, double wallWidth, double wallHeight) {
        GraphicsContext mesh = canvas.getGraphicsContext2D();
        mesh.setStroke(Color.BLACK);
        mesh.setLineWidth(lineWidth);
        drawMesh(mesh, meshSize, wallWidth, wallHeight);
    }

    private void drawMesh(GraphicsContext meshType, double meshSize, double wallWidth, double wallHeight) {
        for (double x = 0; x < wallWidth; x += meshSize) {
            meshType.strokeLine(x, 0, x, wallHeight);
        }

        for (double y = 0; y < wallHeight; y += meshSize) {
            meshType.strokeLine(0, y, wallWidth, y);
        }
    }

    private void setupWallXZ() {
        wallXZ.getTransforms().add(new Rotate(90, Rotate.X_AXIS));
        wallXZ.setTranslateX(-wallXY.getWidth() / 2);
        wallXZ.setTranslateY(wallXY.getHeight() / 2);
    }

    private void setupWallXY() {
        wallXY.setTranslateX(-wallXZ.getWidth() / 2);
        wallXY.setTranslateY(-wallXZ.getHeight() / 4);
        wallXY.setTranslateZ(wallYZ.getWidth());
    }

    private void setupWallYZ() {
        wallYZ.getTransforms().add(new Rotate(90, Rotate.Y_AXIS));
        wallYZ.setTranslateX(wallXZ.getHeight() / 2);
        wallYZ.setTranslateY(-wallXZ.getHeight() / 4);
        wallYZ.setTranslateZ(wallXZ.getWidth());
    }
}