package visualisation.utilities;

import dto.Runway;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import visualisation.Visualisation;


public class RunwayMaker {
    private static final Logger LOGGER = LogManager.getLogger(RunwayMaker.class);
    private final Visualisation visualisation;
    private final Runway runway;
    private final int translationZ;
    private Rectangle runwayBoard;
    private Text runwayLabel;

    public RunwayMaker(Visualisation visualisation, Runway runway) {
        this.visualisation = visualisation;
        this.runway = runway;
        this.translationZ = runway.getName().contains("01") ? 200 : 0;
        makeRunway();
        setupRunway();
        makeRunwayLabel();
        setupRunwayLabel();
    }

    public void setRunway() {
        if (runway.getName().contains("01")) {
            visualisation.setRunway01(runwayBoard);
            visualisation.setRunwayLabel01(runwayLabel);
        } else {
            visualisation.setRunway02(runwayBoard);
            visualisation.setRunwayLabel02(runwayLabel);
        }
    }

    private void makeRunway() {
        runwayBoard = new Rectangle(runway.getDepth() / 10, runway.getWidth() / 10);
        try {
            runwayBoard.setFill(new ImagePattern(new Image("airport-runway.jpeg")));
        } catch (NullPointerException noImage) {
            LOGGER.error("NullPointerException: Failed to load runway image");
            throw new IllegalStateException();
        }
    }

    private void setupRunway() {
        runwayBoard.getTransforms().add(new Rotate(90, Rotate.X_AXIS));
        runwayBoard.getTransforms().add(new Rotate(90, Rotate.Z_AXIS));
        runwayBoard.setTranslateX(visualisation.getWallYZ().getTranslateX());
        runwayBoard.setTranslateY(visualisation.getWallXZ().getTranslateY());
        runwayBoard.setTranslateZ(visualisation.getWallXZ().getTranslateZ() + 400 + translationZ);
    }

    private void makeRunwayLabel() {
        runwayLabel = new Text("Runway " + runway.getName());
        runwayLabel.setFont(new Font(15));
        runwayLabel.setFill(Color.BLACK);
    }

    private void setupRunwayLabel() {
        runwayLabel.translateXProperty().bind(runwayBoard.translateXProperty().subtract(runwayLabel.getBoundsInLocal().getWidth() / 2));
        runwayLabel.translateYProperty().bind(runwayBoard.translateYProperty());
        runwayLabel.translateZProperty().bind(runwayBoard.translateZProperty());
        runwayLabel.setRotationAxis(Rotate.Y_AXIS);
        runwayLabel.getTransforms().add(new Rotate(45, Rotate.Y_AXIS));
        runwayLabel.rotateProperty().bind(visualisation.getAngleY().negate());
    }
}