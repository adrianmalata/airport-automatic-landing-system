package visualisation.utilities;

import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import dto.Coordinate;

import java.util.Random;
import java.util.random.RandomGenerator;

public class PointMaker {

    private PointMaker() {
    }

    public static void makePoint(Point point, Coordinate position) {
        point.setBody(makeBody());
        point.updatePointPosition(position);
        point.setLabel(makeLabel(point));
    }

    public static void errasePoint(Point point) {
        point.getBody().setVisible(false);
        point.getLabel().setVisible(false);
    }

    private static Sphere makeBody() {
        Sphere body = new Sphere(5);
        int[] rgbColors = generateRgbColors();
        body.setMaterial(new PhongMaterial(Color.rgb(rgbColors[0], rgbColors[1], rgbColors[2])));
        return body;
    }

    private static int[] generateRgbColors() {
        Random random = Random.from(RandomGenerator.getDefault());
        int[] rgbColors = new int[3];

        for (int i = 0; i < 3; i++) {
            rgbColors[i] = random.nextInt(0, 256);
        }

        return rgbColors;
    }

    private static Text makeLabel(Point point) {
        Text label = new Text(point.getName());
        label.setFont(new Font(15));
        label.setFill(Color.BLACK);

        translateXYZProperties(point, label);

        label.setRotationAxis(Rotate.Y_AXIS);
        label.getTransforms().add(new Rotate(45, Rotate.Y_AXIS));
        label.rotateProperty().bind(point.getVisualisation().getAngleY().negate());

        return label;
    }

    private static void translateXYZProperties(Point point, Text label) {
        label.translateXProperty().bind(point.getBody().translateXProperty().subtract(label.getBoundsInLocal().getWidth() / 2));
        label.translateYProperty().bind(point.getBody().translateYProperty().add(point.getBody().radiusProperty()).subtract(20));
        label.translateZProperty().bind(point.getBody().translateZProperty());
    }
}