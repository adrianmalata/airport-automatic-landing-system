package visualisation.utilities;

import javafx.scene.shape.Sphere;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Setter;
import dto.Coordinate;
import visualisation.Visualisation;

@Getter
@Setter
public class Point {

    private final Visualisation visualisation;
    private final String name;
    private Sphere body;
    private Text label;


    public Point(Visualisation visualisation, String name) {
        this.visualisation = visualisation;
        this.name = name;
    }

    public void updatePointPosition(Coordinate position) {
        Coordinate correctPosition = correctPosition(position);
        body.setTranslateX(-visualisation.getWallXZ().getWidth() / 2 + correctPosition.x());
        body.setTranslateY(visualisation.getWallXY().getHeight() / 2 - correctPosition.y());
        body.setTranslateZ(correctPosition.z());
    }

    private Coordinate correctPosition(Coordinate position) {
        return new Coordinate(position.x() / 10, position.y() / 10, position.z() / 10);
    }
}