package visualisation.utilities;

import javafx.scene.transform.Rotate;
import visualisation.Visualisation;

public class GroupViewAdjuster {
    private final Visualisation visualisation;

    public GroupViewAdjuster(Visualisation visualisation) {
        this.visualisation = visualisation;
    }

    public void setupAngledView() {
        setupGroupView(
                Visualisation.WINDOW_WIDTH / 4 * 3,
                Visualisation.WINDOW_HEIGHT / 2,
                500,
                new Rotate(-45, Rotate.Y_AXIS)
        );
    }

    private void setupGroupView(double translateX, double translateY, double translateZ, Rotate... rotates) {
        visualisation.getGroup().setTranslateX(translateX);
        visualisation.getGroup().setTranslateY(translateY);
        visualisation.getGroup().setTranslateZ(translateZ);
        visualisation.getGroup().getTransforms().addAll(rotates);
    }
}