package visualisation;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import visualisation.utilities.*;

import static airport.AirportData.RUNWAY_01;
import static airport.AirportData.RUNWAY_02;

@Getter
@Setter
public class Visualisation extends Application {

    // wallXZ – flat, wallXY – front, wallYZ – right

    public static final double WINDOW_WIDTH = 1200;
    public static final double WINDOW_HEIGHT = 800;
    private static final Logger LOGGER = LogManager.getLogger(Visualisation.class);
    private final DoubleProperty angleX = new SimpleDoubleProperty(0);
    private final DoubleProperty angleY = new SimpleDoubleProperty(0);
    private final DoubleProperty angleZ = new SimpleDoubleProperty(0);
    private final Translate translate = new Translate();
    private final Group group;
    private final Scene scene;
    private final GroupViewAdjuster groupViewAdjuster;
    private Rotate xRotate = new Rotate(0, Rotate.X_AXIS);
    private Rotate yRotate = new Rotate(0, Rotate.Y_AXIS);
    private Rotate zRotate = new Rotate(0, Rotate.Z_AXIS);
    private double anchorX;
    private double anchorY;
    private double anchorAngleX = 0;
    private double anchorAngleY = 0;
    private double anchorAngleZ = 0;
    private Rectangle wallXZ;
    private Rectangle wallXY;
    private Rectangle wallYZ;
    private Rectangle runway01;
    private Text runwayLabel01;
    private Rectangle runway02;
    private Text runwayLabel02;
    private View currentView;

    public Visualisation() {
        group = new Group();
        scene = new Scene(group, WINDOW_WIDTH, WINDOW_HEIGHT, Color.WHITE);
        groupViewAdjuster = new GroupViewAdjuster(this);

        WallMaker wallMaker = new WallMaker(this);
        wallMaker.setAllWalls();

        PerspectiveCamera camera = new PerspectiveCamera();
        scene.setCamera(camera);

        RunwayMaker runway01Maker = new RunwayMaker(this, RUNWAY_01);
        runway01Maker.setRunway();

        RunwayMaker runway02Maker = new RunwayMaker(this, RUNWAY_02);
        runway02Maker.setRunway();

        groupViewAdjuster.setupAngledView();

        currentView = View.ANGLED;
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.toFront();
        group.getChildren().addAll(wallXZ, wallXY, wallYZ);
        group.getChildren().addAll(runway01, runwayLabel01, runway02, runwayLabel02);
        group.getTransforms().add(translate);
        initializeRotationsAndControls(group, scene);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Airport automatic landing system");
        primaryStage.show();
    }

    public void addPointToVisualisationGroup(Point point) {
        group.getChildren().addAll(point.getBody(), point.getLabel());
    }

    public void removePointFromVisualisationGroup(Point point) {
        group.getChildren().removeAll(point.getBody(), point.getLabel());
    }

    private void initializeRotationsAndControls(Group group, Scene scene) {
        initializeRotations(group);
        initializeMouseControls(scene);
        initializeKeyboardControls(scene);
    }

    private void initializeRotations(Group group) {
        group.getTransforms().addAll(xRotate, yRotate, zRotate);
        xRotate.angleProperty().bind(angleX);
        yRotate.angleProperty().bind(angleY);
        zRotate.angleProperty().bind(angleZ);
    }

    private void initializeMouseControls(Scene scene) {
        setOnMousePressed(scene);
        setOnMouseDragged(scene);
    }

    private void setOnMousePressed(Scene scene) {
        scene.setOnMousePressed(event -> {
            anchorX = event.getSceneX();
            anchorY = event.getSceneY();
            anchorAngleX = angleX.get();
            anchorAngleY = angleY.get();
            anchorAngleZ = angleZ.get();
        });
    }

    private void setOnMouseDragged(Scene scene) {
        scene.setOnMouseDragged(event -> {
            double deltaX = anchorX - event.getSceneX();
            double deltaY = anchorY - event.getSceneY();
            angleX.set(anchorAngleX - deltaY);
            angleY.set(anchorAngleY + deltaX);
        });
    }

    private void initializeKeyboardControls(Scene scene) {
        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.SPACE) {
                if (currentView == View.ANGLED) {
                    setupView(0, 0, 0);
                    currentView = View.TOP;
                } else if (currentView == View.TOP) {
                    setupView(-45, -90, 250);
                    currentView = View.ANGLED;
                }
            }
        });
    }

    private void setupView(int value, int value1, int value2) {
        angleX.set(0);
        angleY.set(value);
        angleZ.set(value1);
        translate.setX(value2);
    }

    public static void main(String[] args) {
        try {
            launch(args);
        } catch (IllegalStateException illegalState) {
            LOGGER.error("IllegalStateException: Method launch() called more than once or called from the JavaFX application thread");
        } catch (RuntimeException runtime) {
            LOGGER.error("RuntimeException: There was an error launching the JavaFX runtime, or the application class couldn't be constructed, or an Exception or Error was thrown by the Application constructor, init method, start method, or stop method");
        }
    }
}