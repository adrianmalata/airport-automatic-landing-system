package utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public class PropertiesUtilities {
    private static final Logger LOGGER = LogManager.getLogger(PropertiesUtilities.class);
    protected static final Properties PROPERTIES = loadPropertiesFromFile();
    public static final String SERVER_VERSION = PROPERTIES.getProperty("SERVER_VERSION");
    public static final LocalDate SERVER_CREATION_DATE = LocalDate.parse(PROPERTIES.getProperty("SERVER_CREATION_DATE"), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    public static final String CLIENT_IP = PROPERTIES.getProperty("CLIENT_IP");
    public static final int MAIN_COMMUNICATION_PORT = Integer.parseInt(PROPERTIES.getProperty("MAIN_COMMUNICATION_PORT"));
    public static final int SIMULATION_PORT = Integer.parseInt(PROPERTIES.getProperty("SIMULATION_PORT"));
    public static final int AIRPLANE_START_PORT = Integer.parseInt(PROPERTIES.getProperty("AIRPLANE_START_PORT"));
    public static final int NUMBER_OF_AIRPLANES = Integer.parseInt(PROPERTIES.getProperty("NUMBER_OF_AIRPLANES"));

    private PropertiesUtilities() {
    }

    public static Properties loadPropertiesFromFile() {
        Properties properties = new Properties();
        try (InputStream inputStream = new FileInputStream("src/main/resources/config.properties")) {
            properties.load(inputStream);
        } catch (IOException ioException) {
            LOGGER.error("An error occurred while reading the configuration file.");
            throw new IllegalStateException();
        } catch (NumberFormatException numberFormatException) {
            LOGGER.error("Invalid port number in the configuration file.");
            throw new IllegalStateException();
        } catch (NullPointerException nullPointerException) {
            LOGGER.error("inputStream parameter is null.");
        }
        return properties;
    }

}