package airport;

import airplane.AirplaneSystem;
import airport.utilities.MessageManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import dto.Command;

import static org.junit.jupiter.api.Assertions.*;

class AirportSystemTest {
    private AirportSystem airportSystem;
    private AirplaneSystem airplaneSystem;

    @BeforeEach
    void setUp() {
        airportSystem = new AirportSystem();
        airplaneSystem = new AirplaneSystem();
    }

    @Test
    void getGreeting() {
        String greeting = airportSystem.getGreetingOrRejection(airplaneSystem);
        Command command = MessageManager.parseCommand(greeting);
        assertEquals(command.getName(), airplaneSystem.getName());
    }

    @Test
    void getRejection() {
        for (int i=0; i < 100; i++) {
            airportSystem.getAllTraffic().add(new AirplaneSystem());
        }

        String rejection = airportSystem.getGreetingOrRejection(airplaneSystem);
        assertTrue(rejection.contains("Sorry"));
    }

    @Test
    void updateAirplaneInTraffic() {
        airportSystem.getAllTraffic().add(airplaneSystem);
        assertEquals(1, airportSystem.getAllTraffic().size());
        assertFalse(airportSystem.getAllTraffic().get(0).isLandedFlag());
        airplaneSystem.setLandedFlag(true);
        airportSystem.updateAirplaneInTraffic(airplaneSystem);
        assertTrue(airportSystem.getAllTraffic().get(0).isLandedFlag());
    }

    @Test
    void removeAirplaneFromTrafficAtHoldingPattern() {
        AirplaneSystem airplaneSystem1 = new AirplaneSystem();
        AirplaneSystem airplaneSystem2 = new AirplaneSystem();

        airportSystem.addAirplaneToTrafficAtHoldingPattern(1, airplaneSystem1);
        airportSystem.addAirplaneToTrafficAtHoldingPattern(2, airplaneSystem2);

        assertEquals(1, airportSystem.getTrafficAtLevels().get(1).size());
        assertEquals(1, airportSystem.getTrafficAtLevels().get(2).size());

        airportSystem.removeAirplaneFromTrafficAtHoldingPattern(airplaneSystem1);
        airportSystem.removeAirplaneFromTrafficAtHoldingPattern(airplaneSystem2);

        assertEquals(0, airportSystem.getTrafficAtLevels().get(1).size());
        assertEquals(0, airportSystem.getTrafficAtLevels().get(2).size());
    }
}