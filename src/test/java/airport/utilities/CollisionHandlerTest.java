package airport.utilities;

import dto.AirplaneStatus;
import airplane.AirplaneSystem;
import airplane.utilities.CommandExecutor;
import airport.AirportSystem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import dto.Command;
import dto.Coordinate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CollisionHandlerTest {

    // airplane and airplane2- collision, airplane and airplane3- no collision, airplane and airplane4- not found

    private AirportSystem airport;
    private CollisionHandler collisionHandler;
    private AirplaneSystem airplane;
    private AirplaneSystem airplane2;
    private AirplaneSystem airplane3;
    private AirplaneSystem airplane4;
    private List<AirplaneStatus> airplanesNearby;
    private Map<AirplaneStatus, Boolean> possibleCollisions;

    @BeforeEach
    void setUp() {
        airport = new AirportSystem();
        collisionHandler = new CollisionHandler(airport);

        airplane = new AirplaneSystem();
        setupAirplane(airplane, new Coordinate(0, 100, 100), new Coordinate(100, 100, 100), 2);

        airplane2 = new AirplaneSystem();
        setupAirplane(airplane2, new Coordinate(25, 100, 100), new Coordinate(100, 100, 100), 1);

        airplane3 = new AirplaneSystem();
        setupAirplane(airplane3, new Coordinate(50, 100, 100), new Coordinate(50, 100, 0), 2);

        airplane4 = new AirplaneSystem();
        setupAirplane(airplane4, new Coordinate(0, 300, 100), new Coordinate(100, 300, 100), 2);
    }

    @Test
    void scanForOtherAirplanesNearby() {
        calculatePossibleCollisionsWithAllAirplanes();

        assertTrue(airplanesNearby.contains(airplane2));
        assertTrue(airplanesNearby.contains(airplane3));
        assertFalse(airplanesNearby.contains(airplane4));
    }

    @Test
    void scanForCollisions() {
        calculatePossibleCollisionsWithAllAirplanes();
        assertTrue(possibleCollisions.containsValue(true));
        assertEquals(2, possibleCollisions.size());
    }

    @Test
    void preventCollision() {
        calculatePossibleCollisionsWithAllAirplanes();

        Command command = collisionHandler.preventCollision(airplane);

        assertTrue(command.isCollisionProcedureOn());
        assertEquals(airplane.getVelocity() * 0.8, command.getVelocity());
    }

    @Test
    void checkIsCollision() {
        Map<AirplaneStatus, Boolean> possibleCollisions = new HashMap<>();

        AirplaneStatus airplane1 = new AirplaneSystem();
        airplane1.setCurrentPosition(new Coordinate(0, 0, 0));
        AirplaneStatus airplane2 = new AirplaneSystem();
        airplane2.setCurrentPosition(new Coordinate(2, 2, 2));

        possibleCollisions.put(airplane2, true);

        assertTrue(collisionHandler.isCollision(airplane1, possibleCollisions));
    }

    @Test
    void isNoCollision() {
        Map<AirplaneStatus, Boolean> possibleCollisions = new HashMap<>();

        AirplaneStatus airplane1 = new AirplaneSystem();
        airplane1.setCurrentPosition(new Coordinate(0, 0, 0));
        AirplaneStatus airplane2 = new AirplaneSystem();
        airplane2.setCurrentPosition(new Coordinate(11, 11, 11));

        possibleCollisions.put(airplane2, true);

        assertFalse(collisionHandler.isCollision(airplane1, possibleCollisions));
    }

    private void setupAirplane(AirplaneStatus airplane, Coordinate currentPosition, Coordinate destination, double velocity) {
        CommandExecutor commandExecutor = new CommandExecutor(airplane);
        airplane.setCurrentPosition(currentPosition);
        airplane.setDestination(destination);
        airplane.setVelocity(velocity);
        airplane.setVector(commandExecutor.calculateVectorWithDestination(airplane.getDestination()));
    }

    private void calculatePossibleCollisionsWithAllAirplanes() {
        addAllAirplanesToTraffic();
        airplanesNearby = collisionHandler.scanForOtherAirplanesNearby(airplane);
        possibleCollisions = collisionHandler.scanForCollisions(airplane, airplanesNearby);
    }

    private void addAllAirplanesToTraffic() {
        airport.addAirplaneToTraffic(airplane);
        airport.addAirplaneToTraffic(airplane2);
        airport.addAirplaneToTraffic(airplane3);
        airport.addAirplaneToTraffic(airplane4);
    }
}