package airport.utilities;

import airplane.AirplaneSystem;
import airplane.utilities.InitialDestinationCalculator;
import org.junit.jupiter.api.Test;
import dto.Coordinate;

import java.util.List;

import static airport.AirportData.INITIAL_PATTERN;
import static org.junit.jupiter.api.Assertions.*;

class InitialPatternXZCalculatorTest {

    @Test
    void getInitialPatternXZQ1() {
        AirplaneSystem airplane = new AirplaneSystem();
        setCurrentPositionAndDestination(airplane, 10000, 10000);
        List<Coordinate> initialPatternQ1 = InitialPatternXZCalculator.getInitialPatternXZ(airplane);
        assertEquals(INITIAL_PATTERN.getTopLeft().getList(), initialPatternQ1);
    }

    @Test
    void getInitialPatternXZQ3() {
        AirplaneSystem airplane = new AirplaneSystem();
        setCurrentPositionAndDestination(airplane, 0, 10000);
        List<Coordinate> initialPatternQ3 = InitialPatternXZCalculator.getInitialPatternXZ(airplane);
        assertEquals(INITIAL_PATTERN.getBottomLeft().getList(), initialPatternQ3);
    }

    @Test
    void getInitialPatternXZQ4() {
        AirplaneSystem airplane = new AirplaneSystem();
        setCurrentPositionAndDestination(airplane, 0, 0);
        List<Coordinate> initialPatternQ4 = InitialPatternXZCalculator.getInitialPatternXZ(airplane);
        assertEquals(INITIAL_PATTERN.getBottomRight().getList(), initialPatternQ4);
    }

    private static void setCurrentPositionAndDestination(AirplaneSystem airplane, int x, int z) {
        airplane.setCurrentPosition(new Coordinate(x, 100, z));
        airplane.setDestination(InitialDestinationCalculator.calculateInitialDestination(airplane.getCurrentPosition()));
    }


}