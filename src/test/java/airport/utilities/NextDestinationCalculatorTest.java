package airport.utilities;

import airplane.AirplaneSystem;
import airport.AirportSystem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import dto.Command;

import static org.junit.jupiter.api.Assertions.*;

class NextDestinationCalculatorTest {
    private AirportSystem airportSystem;
    private AirplaneSystem airplaneSystem;
    private NextDestinationCalculator nextDestinationCalculator;
    @BeforeEach
    void setUp() {
        airportSystem = new AirportSystem();
        airplaneSystem = new AirplaneSystem();
        airportSystem.addAirplaneToTraffic(airplaneSystem);
        airportSystem.addAirplaneToTrafficAtHoldingPattern(0, airplaneSystem);
        nextDestinationCalculator = new NextDestinationCalculator(airportSystem);
    }

    @Test
    void getNewDestinationCommand() {
        airplaneSystem.setDestinationFlag(true);
        airportSystem.updateAirplaneInTraffic(airplaneSystem);
        Command command = nextDestinationCalculator.getCommand(airplaneSystem);
        boolean sameName = command.getName().equals(airplaneSystem.getName());
        boolean sameCurrentPosition = command.getCurrentPosition().equals(airplaneSystem.getCurrentPosition());
        boolean sameDestination = command.getDestination().equals(airplaneSystem.getDestination());
        boolean sameVelocity = command.getVelocity() == airplaneSystem.getVelocity();
        assertTrue(sameName);
        assertTrue(sameCurrentPosition);
        assertFalse(sameDestination);
        assertFalse(sameVelocity);
    }

    @Test
    void getProceedCollisionAvoidance() {
        airplaneSystem.setDestinationFlag(false);
        airplaneSystem.setCollisionProcedureOn(true);
        Command command = nextDestinationCalculator.getCommand(airplaneSystem);
        boolean sameName = command.getName().equals(airplaneSystem.getName());
        boolean sameCurrentPosition = command.getCurrentPosition().equals(airplaneSystem.getCurrentPosition());
        boolean sameVelocity = command.getVelocity() == airplaneSystem.getVelocity();
        assertTrue(sameName);
        assertTrue(sameCurrentPosition);
        assertFalse(sameVelocity);
    }

    @Test
    void getFlyFurtherCommand() {
        airplaneSystem.setDestinationFlag(false);
        airportSystem.updateAirplaneInTraffic(airplaneSystem);
        Command command = nextDestinationCalculator.getCommand(airplaneSystem);
        boolean sameName = command.getName().equals(airplaneSystem.getName());
        boolean sameCurrentPosition = command.getCurrentPosition().equals(airplaneSystem.getCurrentPosition());
        boolean sameDestination = command.getDestination().equals(airplaneSystem.getDestination());
        boolean sameVelocity = command.getVelocity() == airplaneSystem.getVelocity();
        assertTrue(sameName);
        assertTrue(sameCurrentPosition);
        assertTrue(sameDestination);
        assertFalse(sameVelocity);
    }
}