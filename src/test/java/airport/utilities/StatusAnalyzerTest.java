package airport.utilities;

import airplane.AirplaneSystem;
import airport.AirportSystem;
import org.junit.jupiter.api.Test;
import dto.Command;
import dto.Coordinate;

import static org.junit.jupiter.api.Assertions.*;

class StatusAnalyzerTest {

    @Test
    void checkIfLanded() {
        AirplaneSystem airplaneSystem = new AirplaneSystem();
        airplaneSystem.setCurrentPosition(new Coordinate(0, 0, 0));
        airplaneSystem.setLandedFlag(true);
        airplaneSystem.setVelocity(3);

        AirportSystem airportSystem = new AirportSystem();
        StatusAnalyzer statusAnalyzer = new StatusAnalyzer(airportSystem);

        Command command = statusAnalyzer.analyzeAirplaneStatus(airplaneSystem);
        assertEquals(airplaneSystem.getName(), command.getName());
        assertEquals(0, command.getVelocity());
    }


}