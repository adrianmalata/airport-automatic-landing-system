package airport.utilities;

import dto.AirplaneStatus;
import airplane.AirplaneSystem;
import airplane.utilities.MessageManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageManagerTest {
    private AirplaneStatus airplane;
    private MessageManager airplaneMessageManager;

    @BeforeEach
    void setUp() {
        airplane = new AirplaneSystem();
        airplaneMessageManager = new MessageManager(airplane);
    }

    @Test
    void updateAirplaneStatus() {
        String status = airplane.toString();
        String expectedStatus = airplaneMessageManager.getAirplaneStatusString();
        assertEquals(expectedStatus, status);
    }

    @Test
    void parseAirplaneStatus() {
        String airplaneStatusString = airplaneMessageManager.getAirplaneStatusString();
        AirplaneStatus airplaneStatus = MessageManager.parseAirplaneStatus(airplaneStatusString);
        assertEquals(airplane.toString(), airplaneStatus.toString());
    }

    @Test
    void dontParseNotStatus() {
        String notStatus = "Here is no status";
        assertThrows(IllegalStateException.class, () -> MessageManager.parseAirplaneStatus(notStatus));
    }

}