package airplane.utilities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import dto.Coordinate;

import static airport.AirportData.LANDING_CORRIDOR;
import static org.junit.jupiter.api.Assertions.*;

class InitialPositionGeneratorTest {
    private Coordinate initialPosition;

    @BeforeEach
    void setUp() {
        initialPosition = InitialPositionGenerator.appearOnRadar();
    }

    @Test
    void appearOnRadar() {
        assertNotNull(initialPosition);
    }

    @RepeatedTest(15)
    void isOnBorder() {
        double coordinateX = initialPosition.x();
        double coordinateZ = initialPosition.z();
        boolean onBorder;
        onBorder = coordinateX == 0 || coordinateX == 10000 || coordinateZ == 0 || coordinateZ == 10000;
        assertTrue(onBorder);
    }

    @RepeatedTest(15)
    void isInTravelCorridor() {
        double coordinateY = initialPosition.y();
        boolean inTravelCorridor = coordinateY <= 5000 && coordinateY >= LANDING_CORRIDOR.getHeight();
        assertTrue(inTravelCorridor);
    }
}