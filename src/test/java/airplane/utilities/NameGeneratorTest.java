package airplane.utilities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class NameGeneratorTest {
    private static final Pattern PATTERN = Pattern.compile("[A-Z]{3}\\d{4}");
    private String name;

    @BeforeEach
    void setUp() {
        name = NameGenerator.generateNewAirplaneName();
    }

    @Test
    void generateNewAirplaneName() {
        assertNotNull(name);
    }

    @RepeatedTest(15)
    void checkPattern() {
        assertTrue(PATTERN.matcher(name).matches());
    }

    @RepeatedTest(15)
    void checkPrefixes() {
        List<String> prefixes = NameGenerator.getPREFIXES();
        assertTrue(prefixes.contains(name.substring(0, 3)));
    }
}