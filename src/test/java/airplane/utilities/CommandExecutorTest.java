package airplane.utilities;

import airplane.AirplaneSystem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import dto.Command;
import dto.Coordinate;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static airport.AirportData.getLandingPattern;
import static org.junit.jupiter.api.Assertions.*;

class CommandExecutorTest {
    private AirplaneSystem airplaneSystem;
    private CommandExecutor commandExecutor;

    @BeforeEach
    public void setUp() {
        airplaneSystem = new AirplaneSystem();
        commandExecutor = new CommandExecutor(airplaneSystem);
    }

    @Test
    void executeSimpleCommand() {
        Coordinate currentPosition = new Coordinate(0, 0, 0);
        airplaneSystem.setCurrentPosition(currentPosition);
        Coordinate destination = new Coordinate(200, 200, 200);
        double velocity = 1;
        double[] vector = calculateVector(currentPosition, destination, velocity);
        String name = airplaneSystem.getName();
        boolean collisionProcedureOn = true;
        Command command = new Command(name, currentPosition, destination, velocity, collisionProcedureOn, false);

        commandExecutor.executeCommand(command);

        Coordinate actualCurrentPosition = airplaneSystem.getCurrentPosition();
        Coordinate actualDestination = airplaneSystem.getDestination();
        List<Double> actualVector = airplaneSystem.getVector();
        double actualVelocity = airplaneSystem.getVelocity();
        boolean destinationFlag = airplaneSystem.isDestinationFlag();
        boolean landedFlag = airplaneSystem.isLandedFlag();

        double[] actualCurrentPositionArray = {actualCurrentPosition.x(), actualCurrentPosition.y(), actualCurrentPosition.z()};

        assertEquals(Arrays.toString(vector), actualVector.toString());
        assertEquals(Arrays.toString(vector), Arrays.toString(actualCurrentPositionArray));
        assertEquals(destination, actualDestination);
        assertEquals(1, actualVelocity);
        assertFalse(destinationFlag);
        assertFalse(landedFlag);
    }

    @Test
    void calculateVectorWithDestination() {
        Coordinate currentPosition = new Coordinate(0, 0, 0);
        Coordinate destination = new Coordinate(10, 10, 10);
        double velocity = 2;

        double[] calculatedVector = calculateVector(currentPosition, destination, velocity);

        airplaneSystem.setCurrentPosition(currentPosition);
        airplaneSystem.setVelocity(velocity);
        List<Double> actualVector = commandExecutor.calculateVectorWithDestination(destination);

        assertEquals(Arrays.toString(calculatedVector), actualVector.toString());
    }

    @Test
    void dontSetDestinationAndVectorIfDestinationIsNull() {
        Coordinate firstDestination = airplaneSystem.getDestination();
        List<Double> firstVector = airplaneSystem.getVector();
        boolean collisionProcedureOn = true;

        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), null, airplaneSystem.getVelocity(), collisionProcedureOn, false);
        commandExecutor.executeCommand(command);

        Coordinate secondDestination = airplaneSystem.getDestination();
        List<Double> secondVector = airplaneSystem.getVector();

        assertEquals(firstDestination, secondDestination);
        assertEquals(firstVector, secondVector);
    }

    @Test
    void dontSetVelocityIfVelocityIsNaN() {
        double firstVelocity = airplaneSystem.getVelocity();
        boolean collisionProcedureOn = true;
        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), airplaneSystem.getDestination(), Double.NaN, collisionProcedureOn, false);
        commandExecutor.executeCommand(command);
        double secondVelocity = airplaneSystem.getVelocity();
        assertEquals(firstVelocity, secondVelocity);
    }

    @Test
    void flyWithVector() {
        Coordinate currentPosition = airplaneSystem.getCurrentPosition();
        Coordinate destination = new Coordinate(0, 0, 0);
        int velocity = 10;
        boolean collisionProcedureOn = true;
        Command command = new Command(airplaneSystem.getName(), currentPosition, destination, velocity, collisionProcedureOn, false);
        commandExecutor.executeCommand(command);
        double[] vector = calculateVector(currentPosition, destination, velocity);
        Coordinate expectedPosition = new Coordinate(currentPosition.x() + vector[0], currentPosition.y() + vector[1], currentPosition.z() + vector[2]);
        Coordinate actualPosition = airplaneSystem.getCurrentPosition();
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void changeDestinationFlagNearDestination() {
        assertFalse(airplaneSystem.isDestinationFlag());

        airplaneSystem.setCurrentPosition(new Coordinate(0, 0, 0));
        airplaneSystem.setDestination(new Coordinate(1, 1, 1));
        boolean collisionProcedureOn = true;
        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), airplaneSystem.getDestination(), airplaneSystem.getVelocity(), collisionProcedureOn, false);
        commandExecutor.executeCommand(command);

        assertTrue(airplaneSystem.isDestinationFlag());
    }

    @Test
    void changeDestinationFlagIfNewDestination() {
        airplaneSystem.setDestinationFlag(false);
        airplaneSystem.setCurrentPosition(new Coordinate(0, 0, 0));
        airplaneSystem.setDestination(new Coordinate(6, 6, 6));
        boolean collisionProcedureOn = false;
        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), airplaneSystem.getDestination(), airplaneSystem.getVelocity(), collisionProcedureOn, false);
        commandExecutor.executeCommand(command);
        assertTrue(airplaneSystem.isDestinationFlag());
    }

    @Test
    void dontChangeDestinationFlag() {
        assertFalse(airplaneSystem.isDestinationFlag());

        airplaneSystem.setCurrentPosition(new Coordinate(0, 0, 0));
        airplaneSystem.setDestination(new Coordinate(100, 100, 100));
        boolean collisionProcedureOn = true;
        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), airplaneSystem.getDestination(), airplaneSystem.getVelocity(), collisionProcedureOn, false);
        commandExecutor.executeCommand(command);

        assertFalse(airplaneSystem.isDestinationFlag());
    }

    @Test
    void changeLandedFlag() {
        assertFalse(airplaneSystem.isLandedFlag());
        List<Coordinate> landingPatternList = getLandingPattern(1).getList();

        Coordinate landingPoint = landingPatternList.get(landingPatternList.size() - 1);
        airplaneSystem.setCurrentPosition(new Coordinate(9497, 0, 5999));
        boolean collisionProcedureOn = false;
        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), landingPoint, airplaneSystem.getVelocity(), collisionProcedureOn, false);
        commandExecutor.executeCommand(command);

        assertTrue(airplaneSystem.isLandedFlag());
    }

    @Test
    void dontChangeLandedFlag() {
        assertFalse(airplaneSystem.isLandedFlag());
        List<Coordinate> landingPatternList = getLandingPattern(1).getList();

        Coordinate landingPoint = landingPatternList.get(landingPatternList.size() - 1);
        airplaneSystem.setCurrentPosition(new Coordinate(9390, 0, 4990));
        boolean collisionProcedureOn = true;
        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), landingPoint, airplaneSystem.getVelocity(), collisionProcedureOn, false);
        commandExecutor.executeCommand(command);

        assertFalse(airplaneSystem.isLandedFlag());
    }

    @Test
    void changeCrashedWithFuel() {
        assertFalse(airplaneSystem.isCrashed());

        airplaneSystem.setFuel(LocalTime.of(0, 0, 2));
        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), airplaneSystem.getDestination(), airplaneSystem.getVelocity(), false, false);

        int fullFuelMilliseconds = 2000;
        for (int i = 0; i < fullFuelMilliseconds; i++) {
            commandExecutor.executeCommand(command);
        }

        assertTrue(airplaneSystem.isCrashed());
    }

    @Test
    void reportCrash() {
        assertFalse(airplaneSystem.isCrashed());

        Command command = new Command(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), airplaneSystem.getDestination(), airplaneSystem.getVelocity(), false, true);
        commandExecutor.executeCommand(command);

        assertTrue(airplaneSystem.isCrashed());
    }

    private static double[] calculateVector(Coordinate currentPosition, Coordinate destination, double velocity) {
        double vectorX = destination.x() - currentPosition.x();
        double vectorY = destination.y() - currentPosition.y();
        double vectorZ = destination.z() - currentPosition.z();

        double powX = Math.pow(destination.x() - currentPosition.x(), 2);
        double powY = Math.pow(destination.y() - currentPosition.y(), 2);
        double powZ = Math.pow(destination.z() - currentPosition.z(), 2);

        double distance = Math.sqrt(powX + powY + powZ);

        vectorX /= distance;
        vectorY /= distance;
        vectorZ /= distance;

        vectorX *= velocity;
        vectorY *= velocity;
        vectorZ *= velocity;

        return new double[]{vectorX, vectorY, vectorZ};
    }
}