package airplane.utilities;

import org.junit.jupiter.api.Test;
import dto.Coordinate;

import static org.junit.jupiter.api.Assertions.*;

class InitialDestinationCalculatorTest {

    @Test
    void calculateInitialDestinationQ1() {
        Coordinate initialDestination = InitialDestinationCalculator.calculateInitialDestination(new Coordinate(7000, 4000, 10000));
        assertEquals(new Coordinate(5000, 4000, 9000), initialDestination);
    }

    @Test
    void calculateInitialDestinationQ2() {
        Coordinate initialDestination = InitialDestinationCalculator.calculateInitialDestination(new Coordinate(7000, 4000, 0));
        assertEquals(new Coordinate(5000, 4000, 1000), initialDestination);
    }

    @Test
    void calculateInitialDestinationQ3() {
        Coordinate initialDestination = InitialDestinationCalculator.calculateInitialDestination(new Coordinate(0, 4000, 10000));
        assertEquals(new Coordinate(5000, 4000, 7400), initialDestination);
    }

    @Test
    void calculateInitialDestinationQ4() {
        Coordinate initialDestination = InitialDestinationCalculator.calculateInitialDestination(new Coordinate(0, 4000, 0));
        assertEquals(new Coordinate(1100, 4000, 5000), initialDestination);
    }
}