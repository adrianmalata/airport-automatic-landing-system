package airplane.utilities;

import airplane.AirplaneSystem;
import org.junit.jupiter.api.Test;
import dto.Command;
import dto.Coordinate;

import static org.junit.jupiter.api.Assertions.*;

class MessageManagerTest {
    private final AirplaneSystem airplaneSystem = new AirplaneSystem();

    @Test
    void getCommandString() {
        String name = airplaneSystem.getName();
        Coordinate currentPosition = airplaneSystem.getCurrentPosition();
        Coordinate destination = airplaneSystem.getDestination();
        double velocity = airplaneSystem.getVelocity();
        boolean collisionProcedureOn = true;
        boolean crashed = false;
        Command command = new Command(name, currentPosition, destination, velocity, collisionProcedureOn, crashed);
        String commandString = airport.utilities.MessageManager.getCommandString(command);
        String expectedCommandString = String.format(
                "{\"name\":\"%s\",\"destination\":%s,\"velocity\":%s,\"collisionProcedureOn\":%s,\"crashed\":%s}",
                name, destination.toString(), velocity, collisionProcedureOn, crashed);
        assertEquals(expectedCommandString, commandString);
    }

    @Test
    void parseCommand() {
        String name = "name";
        Coordinate currentPosition = new Coordinate(0, 0, 0);
        Coordinate destination = new Coordinate(0, 0, 0);
        double velocity = 0;
        boolean collisionProcedureOn = true;
        Command command = new Command(name, currentPosition, destination, velocity, collisionProcedureOn, false);

        String commandString = command.toString();
        Command parsedCommand = airport.utilities.MessageManager.parseCommand(commandString);


        assertEquals(name, parsedCommand.getName());
        assertEquals(currentPosition, parsedCommand.getCurrentPosition());
        assertEquals(destination, parsedCommand.getDestination());
        assertEquals(velocity, parsedCommand.getVelocity());
        assertEquals(collisionProcedureOn, parsedCommand.isCollisionProcedureOn());
    }

    @Test
    void dontParseNotCommand() {
        String notCommand = "Here is no command";
        assertThrows(IllegalStateException.class, () -> airport.utilities.MessageManager.parseCommand(notCommand));
    }
}