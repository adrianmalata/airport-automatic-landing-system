package airplane;

import airport.AirportSystem;
import dto.Coordinate;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class AirplaneSystemTest {

    @Test
    void testEquals() {
        AirplaneSystem a1 = new AirplaneSystem();
        a1.setName("Test");
        AirplaneSystem a2 = new AirplaneSystem();
        a2.setName("Test");
        AirplaneSystem a3 = new AirplaneSystem();
        a3.setName("Test1");

        assertEquals(a1, a2);
        assertNotEquals(a2, a3);
    }

    @Test
    void testCalculatingNextPositionAccordingToCurrentVector() {
        AirplaneSystem airplaneSystem = new AirplaneSystem();
        Coordinate mockCurrentPosition = new Coordinate(0, 0, 0);
        airplaneSystem.setCurrentPosition(mockCurrentPosition);

        List<Double> mockVector = List.of(2.0, 2.0, 2.0);
        airplaneSystem.setVector(mockVector);

        assertEquals(new Coordinate(2, 2, 2), airplaneSystem.getNextPosition());
    }

    @Test
    void testTurnAround() {
        AirplaneSystem airplaneSystem = new AirplaneSystem();
        Map.Entry<Boolean, LocalDateTime> hasTurnedAround = airplaneSystem.turnAroundIfNecessary(AirportSystem.REJECTION_MESSAGE.replace("%s", airplaneSystem.getName()));
        assertFalse(hasTurnedAround.getKey());
    }

    @Test
    void testInvitation() {
        AirplaneSystem airplaneSystem = new AirplaneSystem();
        Map.Entry<Boolean, LocalDateTime> invited = airplaneSystem.turnAroundIfNecessary("Some initial response other than rejection");
        assertTrue(invited.getKey());
    }

}