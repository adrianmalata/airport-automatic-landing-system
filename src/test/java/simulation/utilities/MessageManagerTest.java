package simulation.utilities;

import airplane.AirplaneSystem;
import org.junit.jupiter.api.Test;
import dto.PointPosition;

import static org.junit.jupiter.api.Assertions.*;

class MessageManagerTest {

    @Test
    void prepareAndParsePointMessage() {
        AirplaneSystem airplaneSystem = new AirplaneSystem();
        String message = MessageManager.getPointPositionString(airplaneSystem.getName(), airplaneSystem.getCurrentPosition(), airplaneSystem.isLandedFlag(), airplaneSystem.isCrashed());
        PointPosition pointPosition = MessageManager.parsePointMessage(message);
        assertEquals(airplaneSystem.getName(), pointPosition.name());
        assertEquals(airplaneSystem.getCurrentPosition(), pointPosition.currentPosition());
    }
}