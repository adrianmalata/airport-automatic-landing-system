package utilities;

import database.DatabaseService;
import database.entities.AirplaneEntity;
import database.entities.CrashEntity;
import database.entities.GreetingAndRejectionEntity;
import database.entities.LandingEntity;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DatabaseServiceTest {
    private static final DatabaseService databaseService = DatabaseService.getInstance();

    @AfterAll
    static void tearDown() {
        databaseService.close();
    }

    @Test
    @Order(1)
    void testCommitToDatabaseWithTransaction() {
        AirplaneEntity airplaneEntity = prepareAirplaneEntity();
        databaseService.commitToDatabaseWithTransaction(airplaneEntity);
        int numRecords = databaseService.getRecordsFromTable("AirplaneEntity.findAll", AirplaneEntity.class).size();
        assertNotEquals(0, numRecords);
    }

    @Test
    @Order(2)
    void testFindAirplaneByName() {
        AirplaneEntity airplaneEntity = databaseService.findAirplaneByName("TEST");
        assertNotNull(airplaneEntity);
    }

    @Test
    @Order(3)
    void testCleanTable() {
        int numRecords = databaseService.getRecordsFromTable("CrashEntity.findAll", CrashEntity.class).size();
        assertNotEquals(0, numRecords);
        databaseService.cleanTable("CrashEntity.deleteAll");
        numRecords = databaseService.getRecordsFromTable("CrashEntity.findAll", CrashEntity.class).size();
        assertEquals(0, numRecords);
    }

    @Test
    @Order(4)
    void testCleanAllTables() {
        int numRecords = databaseService.getRecordsFromTable("AirplaneEntity.findAll", AirplaneEntity.class).size();
        assertNotEquals(0, numRecords);
        databaseService.cleanAllTables();
        numRecords = databaseService.getRecordsFromTable("AirplaneEntity.findAll", AirplaneEntity.class).size();
        assertEquals(0, numRecords);
    }

    private AirplaneEntity prepareAirplaneEntity() {
        AirplaneEntity airplaneEntity = new AirplaneEntity();
        String name = "TEST";
        GreetingAndRejectionEntity greetingAndRejectionEntity = prepareGreetingAndRejection(airplaneEntity);
        CrashEntity crashEntity = prepareCrashEntity(airplaneEntity);
        LandingEntity landingEntity = prepareLandingEntity(airplaneEntity);

        airplaneEntity.setAirplaneEntity(name, greetingAndRejectionEntity, crashEntity, landingEntity);
        return airplaneEntity;
    }

    private GreetingAndRejectionEntity prepareGreetingAndRejection(AirplaneEntity airplaneEntity) {
        GreetingAndRejectionEntity greetingAndRejectionEntity = new GreetingAndRejectionEntity();
        LocalDateTime eventDt = LocalDateTime.now();
        int port = 666;
        Boolean greeted = true;

        greetingAndRejectionEntity.setGreetingAndRejectionEntity(eventDt, airplaneEntity, port, greeted);
        return greetingAndRejectionEntity;
    }

    private CrashEntity prepareCrashEntity(AirplaneEntity airplaneEntity) {
        CrashEntity crashEntity = new CrashEntity();
        LocalDateTime crashTime = LocalDateTime.now();

        crashEntity.setCrashEntity(crashTime, airplaneEntity);
        return crashEntity;
    }

    private LandingEntity prepareLandingEntity(AirplaneEntity airplaneEntity) {
        LandingEntity landingEntity = new LandingEntity();
        LocalDateTime landingTime = LocalDateTime.now();

        landingEntity.setLandingEntity(landingTime, airplaneEntity);
        return landingEntity;
    }
}