# Airport automatic landing system

***

### Link

https://youtu.be/nf06ISk0gaY

### Type

Multithread client-server architecture

### Description

* Airplanes and simulation are clients, airport is server
* The airport has 2 lanes, space 10x10x5 km, air corridor 5x1x2 km
* In the airspace of the airport can be a maximum of 100 airplanes, each additional one receives message about the impossibility of landing
* The airplane has fuel for a 3-hour flight
* If two airplanes come within distance of 10 m, a collision occurs
* If an airplane runs out of fuel, a collision occurs
* Information about airport-airplane connections, collisions and successful landings saved in the database
* The goal is to develop a system that brings all planes to the ground safely
* Real-time 3D visualisation in JavaFX

### Guide

Check the settings of socket connection (port, etc.)
Setup database configuration

### Libraries

* Hibernate 6.5.0
* Jackson 2.16.0
* JavaFX 20.0.0
* JPA 3.1.0
* JUnit Jupiter 5.10.1 
* Lombok 1.18.30
* PostgreSQL 42.7.3

***